## Справка по классу `ConfigHelper`

### Описание
Класс `ConfigHelper` предназначен для работы с конфигурационными файлами. Он позволяет читать, изменять и удалять значения конфигурации, хранящиеся в PHP файлах.

### Методы

#### 1. `getConfigArray(string $config_name): array`
Возвращает массив из конфигурационного файла.

##### Параметры:
- **$config_name**: *string* — Имя конфигурационного файла (без расширения).

##### Возвращает:
- *array* — Массив конфигурации или пустой массив, если файл не найден.

##### Пример использования:
```php
$config = ConfigHelper::getConfigArray('database');
print_r($config);
```

---

#### 2. `getConfigValue(string $config_name, string $key, $default = null)`
Возвращает значение из конфигурационного файла по ключу.

##### Параметры:
- **$config_name**: *string* — Имя конфигурационного файла (без расширения).
- **$key**: *string* — Ключ конфигурации.
- **$default**: *mixed* — Значение по умолчанию, если ключ не найден.

##### Возвращает:
- *mixed* — Значение конфигурации или значение по умолчанию, если ключ отсутствует.

##### Пример использования:
```php
$db_host = ConfigHelper::getConfigValue('database', 'host', 'localhost');
echo $db_host;
```

---

#### 3. `hasConfigKey(string $config_name, string $key): bool`
Проверяет наличие ключа в конфигурационном файле.

##### Параметры:
- **$config_name**: *string* — Имя конфигурационного файла (без расширения).
- **$key**: *string* — Ключ конфигурации.

##### Возвращает:
- *bool* — Возвращает `true`, если ключ существует, иначе `false`.

##### Пример использования:
```php
if (ConfigHelper::hasConfigKey('database', 'host')) {
    echo 'Ключ существует';
}
```

---

#### 4. `setConfigValue(string $config_name, string $key, $value): bool`
Обновляет значение конфигурационного файла.

##### Параметры:
- **$config_name**: *string* — Имя конфигурационного файла (без расширения).
- **$key**: *string* — Ключ для обновления.
- **$value**: *mixed* — Новое значение для ключа.

##### Возвращает:
- *bool* — Возвращает `true`, если файл был успешно обновлен.

##### Пример использования:
```php
ConfigHelper::setConfigValue('database', 'host', '127.0.0.1');
```

---

#### 5. `removeConfigKey(string $config_name, string $key): bool`
Удаляет ключ из конфигурационного файла.

##### Параметры:
- **$config_name**: *string* — Имя конфигурационного файла (без расширения).
- **$key**: *string* — Ключ для удаления.

##### Возвращает:
- *bool* — Возвращает `true`, если ключ был удален и файл успешно обновлен.

##### Пример использования:
```php
ConfigHelper::removeConfigKey('database', 'username');
```

---

### Примечания
- Конфигурационные файлы должны быть в формате PHP и возвращать массив.
- Все изменения, сделанные методами `setConfigValue` и `removeConfigKey`, перезаписывают файл конфигурации.

### Пример конфигурационного файла `database.php`:
```php
<?php
return [
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'dbname' => 'test_db',
];
```