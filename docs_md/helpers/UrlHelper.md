# UrlHelper - Хелпер для работы с урлами

`UrlHelper` — это класс, предназначенный для работы с URL. Он предоставляет методы для генерации URL, проверки их валидности, добавления параметров и других операций.

## Методы

### 1. `getProtocol()`

Возвращает протокол для генерации URL (`http` или `https`).

- **Описание:** Этот метод проверяет, определена ли константа `PROTOCOL`. Если она указана как `http` или `https`, возвращает её значение. В противном случае возвращает протокол по умолчанию — `http`.
  
- **Возвращаемое значение:** `string` — Протокол (`http` или `https`).

#### Пример использования:

```php
$protocol = UrlHelper::getProtocol();
echo $protocol; // Например, 'http' или 'https'
```

### 2. `makeUrl(array $params = [], $component_class = false)`

Генерирует URL на основе заданных параметров.

- **Параметры:**
  - `array $params` — Ассоциативный массив с параметрами:
    - `path` (array): части пути URL.
    - `query` (array): параметры запроса (query string).
    - `with_host` (bool): добавлять ли хост в URL.
  - `$component_class` (string|bool) — Класс компонента для добавления в URL (необязательно).

- **Возвращаемое значение:** `string` — Сформированный URL.

#### Пример использования:

```php
$url = UrlHelper::makeUrl([
    'path' => ['user', 'profile'],
    'query' => ['id' => 42],
    'with_host' => true
]);
echo $url; // Например, 'http://example.com/user/profile?id=42'
```

### 3. `makeUrlCurrentComponent(array $params = [])`

Генерирует URL для текущего компонента приложения.

- **Описание:** Этот метод использует параметры для генерации URL для текущего компонента приложения.

- **Параметры:** Аналогичны параметрам метода `makeUrl()`.

- **Возвращаемое значение:** `string` — Сформированный URL.

#### Пример использования:

```php
$currentComponentUrl = UrlHelper::makeUrlCurrentComponent([
    'path' => ['dashboard'],
    'query' => ['section' => 'stats']
]);
echo $currentComponentUrl; // Например, '/dashboard?section=stats'
```

### 4. `isValidUrl(string $url)`

Проверяет валидность URL.

- **Описание:** Этот метод проверяет, является ли переданная строка валидным URL.

- **Параметры:**
  - `string $url` — URL для проверки.

- **Возвращаемое значение:** `bool` — `true`, если URL валиден, иначе `false`.

#### Пример использования:

```php
$isValid = UrlHelper::isValidUrl('https://example.com');
var_dump($isValid); // bool(true)
```

### 5. `addOrReplaceParams(string $url, array $new_params)`

Добавляет или заменяет параметры в существующем URL.

- **Описание:** Метод позволяет добавить или заменить параметры в существующем URL.

- **Параметры:**
  - `string $url` — URL, который нужно модифицировать.
  - `array $new_params` — Параметры для добавления или замены.

- **Возвращаемое значение:** `string` — Модифицированный URL.

#### Пример использования:

```php
$updatedUrl = UrlHelper::addOrReplaceParams('https://example.com/page?foo=1', ['bar' => '2']);
echo $updatedUrl; // 'https://example.com/page?foo=1&bar=2'
```

### 6. `getComponentUrlSafe(string $component_class)`

Возвращает URL для указанного компонента, если это допустимый компонент.

- **Описание:** Метод проверяет, является ли указанный класс наследником компонента. Если это так, возвращает URL компонента.

- **Параметры:**
  - `string $component_class` — Класс компонента.

- **Возвращаемое значение:** `string` — URL компонента.

- **Исключение:** Выбрасывает исключение, если класс не является наследником компонента.

#### Пример использования:

```php
$componentUrl = UrlHelper::getComponentUrlSafe('SomeComponentClass');
echo $componentUrl; // Например, '/component/some'
```

## Заключение

Класс `UrlHelper` предоставляет удобные методы для генерации, проверки и модификации URL, а также работы с компонентами приложения.