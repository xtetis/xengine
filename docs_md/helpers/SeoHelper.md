# SeoHelper: Хелпер для работы с SEO параметрами страницы

### Класс `SeoHelper` предназначен для удобной работы с основными SEO параметрами страницы: Title, Description, Canonical, H1 и Keywords. 

## Методы

### 1. `getPageName()`
Получает заголовок страницы (обычно используется как H1).

```php
SeoHelper::getPageName();
```

#### Возвращаемое значение:
- **string**: Значение параметра H1.

---

### 2. `setPageName(string $name = '', bool $overwrite = true)`
Устанавливает заголовок страницы (H1). 

```php
SeoHelper::setPageName('Заголовок страницы', true);
```

#### Параметры:
- **$name** (string): Заголовок страницы (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующего значения (по умолчанию `true`).

---

### 3. `getTitle()`
Получает заголовок Title страницы.

```php
SeoHelper::getTitle();
```

#### Возвращаемое значение:
- **string**: Текущий Title страницы.

---

### 4. `setTitle(string $title = '', bool $overwrite = true)`
Устанавливает заголовок Title страницы.

```php
SeoHelper::setTitle('Новый Title', true);
```

#### Параметры:
- **$title** (string): Заголовок страницы (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующего значения (по умолчанию `true`).

---

### 5. `getDescription()`
Получает мета-тег Description страницы.

```php
SeoHelper::getDescription();
```

#### Возвращаемое значение:
- **string**: Описание страницы.

---

### 6. `setDescription(string $description = '', bool $overwrite = true)`
Устанавливает мета-тег Description страницы.

```php
SeoHelper::setDescription('Описание страницы', true);
```

#### Параметры:
- **$description** (string): Описание страницы (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующего значения (по умолчанию `true`).

---

### 7. `getCanonical()`
Получает Canonical URL страницы.

```php
SeoHelper::getCanonical();
```

#### Возвращаемое значение:
- **string**: Canonical URL.

---

### 8. `setCanonical(string $url = '', bool $overwrite = true)`
Устанавливает Canonical URL страницы с проверкой корректности URL.

```php
SeoHelper::setCanonical('https://example.com', true);
```

#### Параметры:
- **$url** (string): URL страницы (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующего значения (по умолчанию `true`).

#### Исключения:
- **\InvalidArgumentException**: Если URL некорректен.

---

### 9. `getKeywords()`
Получает ключевые слова страницы.

```php
SeoHelper::getKeywords();
```

#### Возвращаемое значение:
- **string**: Ключевые слова страницы.

---

### 10. `setKeywords(string $keywords = '', bool $overwrite = true)`
Устанавливает ключевые слова страницы.

```php
SeoHelper::setKeywords('ключевое слово, другое ключевое слово', true);
```

#### Параметры:
- **$keywords** (string): Ключевые слова (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующего значения (по умолчанию `true`).

---

### 11. `clearSeoParameters()`
Очищает все SEO параметры страницы: Title, Description, Canonical, H1, Keywords.

```php
SeoHelper::clearSeoParameters();
```

---

### 12. `setMetaTags(string $title = '', string $description = '', string $canonical = '', string $keywords = '', bool $overwrite = true)`
Устанавливает все мета-теги страницы за один вызов.

```php
SeoHelper::setMetaTags(
    'Новый Title',
    'Новое описание',
    'https://example.com',
    'ключевые слова',
    true
);
```

#### Параметры:
- **$title** (string): Заголовок страницы (по умолчанию пустая строка).
- **$description** (string): Описание страницы (по умолчанию пустая строка).
- **$canonical** (string): Canonical URL страницы (по умолчанию пустая строка).
- **$keywords** (string): Ключевые слова страницы (по умолчанию пустая строка).
- **$overwrite** (bool): Флаг, указывающий на возможность перезаписи существующих значений (по умолчанию `true`).