# PaginateHelper

`PaginateHelper` — это класс, предназначенный для генерации HTML-разметки пагинации. Он помогает создавать ссылки на страницы, когда данные разбиваются на несколько страниц.

## Методы

### getPagination

Этот метод генерирует HTML для пагинации на основе количества записей, текущей страницы и общего количества элементов на одной странице.

#### Сигнатура метода

```php
public static function getPagination(
    int $items_per_page, 
    int $current_page, 
    int $total_records, 
    string $page_url
): string
```

#### Аргументы

- **$items_per_page** (int) — Количество элементов на одной странице.
- **$current_page** (int) — Текущая страница.
- **$total_records** (int) — Общее количество записей.
- **$page_url** (string) — URL, на который будут ссылаться элементы пагинации.

#### Возвращаемое значение

- **string** — HTML-разметка с элементами пагинации.

#### Пример вызова

```php
use xtetis\xengine\helpers\PaginateHelper;

$items_per_page = 10;
$current_page = 3;
$total_records = 100;
$page_url = '/products';

// Получение HTML-разметки для пагинации
$pagination_html = PaginateHelper::getPagination($items_per_page, $current_page, $total_records, $page_url);

echo $pagination_html;
```

#### Пример результата

```html
<ul class="pagination">
    <li class="page-item"><a class="page-link" href="/products?p=1" title="First">&laquo;</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=2" title="Previous">&lt;</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=1">1</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=2">2</a></li>
    <li class="page-item active"><a class="page-link">3</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=4">4</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=5">5</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=4">&gt;</a></li>
    <li class="page-item"><a class="page-link" href="/products?p=10" title="Last">&raquo;</a></li>
</ul>
```

## Описание работы

1. **Проверка аргументов:** Метод принимает количество элементов на странице, текущую страницу, общее количество записей и базовый URL.
   
2. **Создание ссылок:** Он проверяет, если общее количество страниц больше одной и если текущая страница корректна. Далее создаются ссылки на первую, предыдущую, текущую, следующую и последнюю страницы.

3. **Оптимизация URL:** В зависимости от того, есть ли уже параметры в URL, метод добавляет `?` или `&` перед параметрами для корректной генерации ссылок.

4. **Активная страница:** Текущая страница выделяется с помощью CSS-класса `active`, чтобы показать пользователю, на какой странице он находится.

## Примечания

- Если текущая страница больше общего количества страниц, пагинация не будет отображена.
- Параметры URL корректно добавляются, если уже присутствует символ `?`.