# Справка по `StatResHelper`

Хелпер `StatResHelper` предназначен для работы со статическими ресурсами, такими как JavaScript и CSS файлы. Он упрощает добавление, получение списка и очистку статических ресурсов для шаблона.

## Методы

### `addTemplateJs(string $filename): bool`

Добавляет JavaScript файл в шаблон.

**Параметры:**
- `string $filename`: Полный путь к JS файлу.

**Возвращает:**
- `bool`: Возвращает `true`, если файл успешно добавлен, и `false`, если произошла ошибка (например, файл не найден или это не JS файл).

**Пример использования:**
```php
$filename = '/path/to/file.js';
StatResHelper::addTemplateJs($filename);
```

### `getTemplateJsList(): string`

Возвращает список добавленных JS файлов в виде HTML-кода, который включает теги `<script>`.

**Возвращает:**
- `string`: HTML код с подключёнными JS файлами.

**Пример использования:**
```php
echo StatResHelper::getTemplateJsList();
```

### `addTemplateCss(string $filename): bool`

Добавляет CSS файл в шаблон.

**Параметры:**
- `string $filename`: Полный путь к CSS файлу.

**Возвращает:**
- `bool`: Возвращает `true`, если файл успешно добавлен, и `false`, если произошла ошибка (например, файл не найден или это не CSS файл).

**Пример использования:**
```php
$filename = '/path/to/file.css';
StatResHelper::addTemplateCss($filename);
```

### `getTemplateCssList(): string`

Возвращает список добавленных CSS файлов в виде HTML-кода, который включает теги `<link>`.

**Возвращает:**
- `string`: HTML код с подключёнными CSS файлами.

**Пример использования:**
```php
echo StatResHelper::getTemplateCssList();
```

### `clearStaticResources(): void`

Очищает все статические ресурсы — как JS, так и CSS файлы.

**Пример использования:**
```php
StatResHelper::clearStaticResources();
```

### `clearStaticJs(): void`

Очищает список добавленных JS файлов.

**Пример использования:**
```php
StatResHelper::clearStaticJs();
```

### `clearStaticCss(): void`

Очищает список добавленных CSS файлов.

**Пример использования:**
```php
StatResHelper::clearStaticCss();
```

## Общая информация

Этот класс используется для работы со статическими файлами, которые добавляются в шаблон, и может быть полезен для управления статическими ресурсами в динамических веб-приложениях.