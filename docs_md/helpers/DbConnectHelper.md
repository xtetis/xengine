```markdown
# Модель `DbConnectHelper`

Класс `DbConnectHelper` предоставляет методы для подключения к базе данных через PDO или PostgreSQL. Он использует конфигурационные параметры для установки соединений с различными базами данных.

## Пространство имен

```php
namespace xtetis\xengine\helpers;
```

## Методы

### 1. `getConnect`

```php
public static function getConnect($connect_name = '')
```

**Описание**: Этот метод возвращает подключение к базе данных, используя PDO. Он является оберткой для метода `getConnectPdo`.

**Параметры**:
- `$connect_name`: Имя подключения (по умолчанию — пустая строка).

**Возвращает**: Объект подключения PDO.

**Пример использования**:

```php
use xtetis\xengine\helpers\DbConnectHelper;

// Получение подключения к базе данных
$dbh = DbConnectHelper::getConnect('default');
```

### 2. `getConnectPdo`

```php
public static function getConnectPdo($connect_name = '')
```

**Описание**: Возвращает подключение к базе данных через PDO, используя конфигурационные параметры для создания DSN. Если соединение уже установлено, возвращает его из глобального массива `$xengine_pdo_connect`.

**Параметры**:
- `$connect_name`: Имя подключения (по умолчанию — пустая строка).

**Возвращает**: Объект подключения PDO.

**Пример использования**:

```php
use xtetis\xengine\helpers\DbConnectHelper;

// Получение PDO подключения
$pdo = DbConnectHelper::getConnectPdo('default');
```

**Особенности**:
- Метод использует глобальный массив `$xengine_pdo_connect` для хранения активных соединений.
- Если соединение не установлено, оно создается, используя параметры конфигурации, такие как `host`, `port`, `dbname`, `user` и `password`.

### 3. `getConnectPg`

```php
public static function getConnectPg($connect_name = '')
```

**Описание**: Возвращает подключение к базе данных PostgreSQL с использованием функции `pg_connect`. Если соединение уже установлено, возвращает его из глобального массива `$xengine_pg_connect`.

**Параметры**:
- `$connect_name`: Имя подключения (по умолчанию — пустая строка).

**Возвращает**: Ресурс подключения PostgreSQL.

**Пример использования**:

```php
use xtetis\xengine\helpers\DbConnectHelper;

// Получение подключения к PostgreSQL
$pgConnect = DbConnectHelper::getConnectPg('pg_default');
```

**Особенности**:
- Метод использует глобальный массив `$xengine_pg_connect` для хранения активных соединений.
- Если соединение не установлено, оно создается с помощью строки подключения, сформированной на основе конфигурационных параметров.

## Примечания

- Методы подключения используют глобальные массивы `$xengine_pdo_connect` и `$xengine_pg_connect` для кеширования соединений, что предотвращает их повторное создание.
- Все параметры для подключения, такие как `host`, `port`, `dbname`, `user` и `password`, загружаются из конфигурационного файла с помощью метода `ConfigHelper::getConfigArray`.
- В случае ошибки при создании подключения программа завершится с сообщением об ошибке.

## Пример конфигурации базы данных

Пример структуры конфигурации, используемой методами подключения:

```php
[
    'params' => [
        'db_connections' => [
            'default' => [
                'host' => 'localhost',
                'port' => 3306,
                'dbname' => 'my_database',
                'user' => 'db_user',
                'password' => 'db_password',
                'type' => 'mysql'
            ],
            'pg_default' => [
                'host' => 'localhost',
                'port' => 5432,
                'dbname' => 'pg_database',
                'user' => 'pg_user',
                'password' => 'pg_password'
            ]
        ]
    ]
]
```
```