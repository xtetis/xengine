<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $reflect                          = new \ReflectionClass($exception);
    $short_class_exception_name = $reflect->getShortName();
    
?>
<!doctype html>
<html lang="en"
      class="h-100">

    <head>
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossorigin="anonymous"></script>

        <title>Ошибка</title>
        <style>

        </style>


    </head>

    <body>
        <div class="container pt-2">
            <h3>
                <?=$short_class_exception_name?>: <?=$exception->getMessage()?>
            </h3>

            <div>
                <b>
                    Описание ошибки
                </b>
            </div>

            <ul class="list-group">
                <li class="list-group-item">
                    <table class="table table-striped table-danger ">
                        <thead>
                            <tr>
                                <th>Параметр</th>
                                <th>Значение</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Файл
                                </td>
                                <td>
                                    <?=$exception->getFile()?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Строка
                                </td>
                                <td>
                                    <?=$exception->getLine()?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Сообщение
                                </td>
                                <td>
                                    <?=$exception->getMessage()?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Код ошибки
                                </td>
                                <td>
                                    <?=$exception->getCode()?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>
            </ul>
            <br>
            <div>
                <b>
                    Стек вызовов
                </b>
            </div>
            <div>
                <ul class="list-group">
                    <?php foreach ($exception->getTrace() as $trace_item):?>
                    <li class="list-group-item">
                        <?php if (count($trace_item) > 0): ?>
                        <table class="table table-striped table-success ">
                            <thead>
                                <tr>
                                    <th>Параметр</th>
                                    <th>Значение</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($trace_item as $k => $v):?>
                                <tr>
                                    <td><?=$k?></td>
                                    <td><?=$v?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php endif; ?>
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </body>

</html>
