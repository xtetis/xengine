<?php

namespace xtetis\xengine\helpers;

/**
 * Класс RequestHelper предоставляет методы для фильтрации и обработки входящих GET, POST и REQUEST запросов.
 */
class RequestHelper
{
    /**
     * Получение и фильтрация GET запроса.
     * 
     * @param string $name Имя параметра запроса.
     * @param string $type Тип данных ('int', 'str', 'struser', 'raw', 'float', 'intfloat').
     * @param mixed $default Значение по умолчанию, если параметр не передан.
     * @return mixed Отфильтрованное значение GET параметра.
     */
    public static function get(string $name, string $type, $default = '')
    {
        // Проверяем корректность указанного типа данных
        if (!self::isValidType($type)) {
            \xtetis\xengine\helpers\LogHelper::customDie('Некорректный тип данных');
        }

        // Получаем значение из массива $_GET или значение по умолчанию
        $value = $_GET[$name] ?? $default;

        // Возвращаем отфильтрованное значение в зависимости от типа данных
        return self::filterValue($value, $type);
    }

    /**
     * Получение и фильтрация POST запроса.
     * 
     * @param string $name Имя параметра запроса.
     * @param string $type Тип данных ('int', 'str', 'struser', 'raw', 'float', 'intfloat').
     * @param mixed $default Значение по умолчанию, если параметр не передан.
     * @return mixed Отфильтрованное значение POST параметра.
     */
    public static function post(string $name, string $type, $default = '')
    {
        // Проверяем корректность указанного типа данных
        if (!self::isValidType($type)) {
            \xtetis\xengine\helpers\LogHelper::customDie('Некорректный тип данных');
        }

        // Получаем значение из массива $_POST или значение по умолчанию
        $value = $_POST[$name] ?? $default;

        // Возвращаем отфильтрованное значение в зависимости от типа данных
        return self::filterValue($value, $type);
    }

    /**
     * Получение и фильтрация REQUEST запроса (может быть GET или POST).
     * 
     * @param string $name Имя параметра запроса.
     * @param string $type Тип данных ('int', 'str', 'struser', 'raw', 'float', 'intfloat').
     * @param mixed $default Значение по умолчанию, если параметр не передан.
     * @return mixed Отфильтрованное значение REQUEST параметра.
     */
    public static function request(string $name, string $type, $default = '')
    {
        // Проверяем корректность указанного типа данных
        if (!self::isValidType($type)) {
            \xtetis\xengine\helpers\LogHelper::customDie('Некорректный тип данных');
        }

        // Получаем значение из массива $_REQUEST или значение по умолчанию
        $value = $_REQUEST[$name] ?? $default;

        // Возвращаем отфильтрованное значение в зависимости от типа данных
        return self::filterValue($value, $type);
    }

    /**
     * Проверка корректности типа данных.
     * 
     * @param string $type Тип данных ('int', 'str', 'struser', 'raw', 'float', 'intfloat').
     * @return bool Возвращает true, если тип данных допустим.
     */
    private static function isValidType(string $type): bool
    {
        // Список допустимых типов данных
        $validTypes = ['int', 'str', 'struser', 'raw', 'float', 'intfloat'];
        // Проверка на соответствие допустимым типам
        return in_array($type, $validTypes, true);
    }

    /**
     * Фильтрация значения на основе типа.
     * 
     * @param mixed $value Значение для фильтрации.
     * @param string $type Тип данных для фильтрации ('int', 'float', 'str', 'struser', 'raw').
     * @return mixed Отфильтрованное значение.
     */
    private static function filterValue($value, string $type)
    {
        // Определяем фильтрацию в зависимости от типа данных
        switch ($type) {
            case 'int':
                return intval($value); // Преобразование в целое число
            case 'float':
                return floatval($value); // Преобразование в число с плавающей точкой
            case 'intfloat':
                return intval(floatval($value)); // Преобразование к целому числу после конвертации в float
            case 'str':
                return strval($value); // Преобразование в строку
            case 'struser':
                // Фильтрация пользовательского ввода: удаление HTML-тегов и экранирование спецсимволов
                return htmlentities(strip_tags(strval($value)), ENT_QUOTES, 'UTF-8');
            case 'raw':
            default:
                return $value; // Возвращаем исходное значение
        }
    }

    /**
     * Проверка существования GET-параметра.
     * 
     * @param string $name Имя GET-параметра.
     * @return bool Возвращает true, если параметр существует.
     */
    public static function hasGet(string $name): bool
    {
        return isset($_GET[$name]);
    }

    /**
     * Проверка существования POST-параметра.
     * 
     * @param string $name Имя POST-параметра.
     * @return bool Возвращает true, если параметр существует.
     */
    public static function hasPost(string $name): bool
    {
        return isset($_POST[$name]);
    }

    /**
     * Проверка существования REQUEST-параметра (GET или POST).
     * 
     * @param string $name Имя REQUEST-параметра.
     * @return bool Возвращает true, если параметр существует.
     */
    public static function hasRequest(string $name): bool
    {
        return isset($_REQUEST[$name]);
    }

    /**
     * Проверка, был ли запрос сделан через AJAX.
     * 
     * @return bool Возвращает true, если запрос был сделан через AJAX.
     */
    public static function isAjax(): bool
    {
        // Проверяем наличие заголовка HTTP_X_REQUESTED_WITH и соответствие его значению 'xmlhttprequest'
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }

    /**
     * Получение всех параметров GET или POST запросов.
     * 
     * @return array Массив параметров из REQUEST.
     */
    public static function getAllParams(): array
    {
        return $_REQUEST; // Возвращаем все параметры, переданные через GET или POST
    }
}
