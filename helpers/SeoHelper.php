<?php
namespace xtetis\xengine\helpers;

/**
 * Хелпер для работы с SEO параметрами страницы
 */
class SeoHelper
{
    /**
     * Получает заголовок страницы (обычно используется как H1).
     *
     * @return string Значение параметра H1.
     */
    public static function getPageName(): string
    {
        // Получаем параметр 'h1', если он не установлен — возвращаем пустую строку.
        return \xtetis\xengine\App::getParam('h1', '');
    }

    /**
     * Устанавливает заголовок страницы (обычно используется как H1).
     *
     * @param string $name      Заголовок страницы.
     * @param bool   $overwrite Флаг, указывающий на возможность перезаписи существующего значения.
     */
    public static function setPageName(
        string $name = '',
        bool   $overwrite = true
    ): void
    {
        // Ограничиваем длину заголовка до 128 символов.
        $name = mb_substr($name, 0, 128);

        // Устанавливаем параметр 'h1' с возможностью перезаписи.
        \xtetis\xengine\App::setParam('h1', $name, $overwrite);
    }

    /**
     * Получает заголовок Title страницы.
     *
     * @return string Текущий Title страницы.
     */
    public static function getTitle(): string
    {
        return \xtetis\xengine\App::getParam('title', '');
    }

    /**
     * Устанавливает Title страницы.
     *
     * @param string $title     Заголовок страницы.
     * @param bool   $overwrite Флаг, указывающий на возможность перезаписи существующего значения.
     */
    public static function setTitle(
        string $title = '',
        bool   $overwrite = true
    ): void
    {
        // Ограничиваем длину Title до 128 символов.
        $title = mb_substr($title, 0, 128);
        \xtetis\xengine\App::setParam('title', $title, $overwrite);
    }

    /**
     * Получает мета-тег Description страницы.
     *
     * @return string Описание страницы.
     */
    public static function getDescription(): string
    {
        return \xtetis\xengine\App::getParam('description', '');
    }

    /**
     * Устанавливает мета-тег Description страницы.
     *
     * @param string $description Описание страницы.
     * @param bool   $overwrite   Флаг, указывающий на возможность перезаписи существующего значения.
     */
    public static function setDescription(
        string $description = '',
        bool   $overwrite = true
    ): void
    {
        // Ограничиваем длину Description до 200 символов.
        $description = mb_substr($description, 0, 200);
        \xtetis\xengine\App::setParam('description', $description, $overwrite);
    }

    /**
     * Устанавливает canonical URL для страницы с проверкой корректности URL.
     *
     * @param  string                    $url       URL страницы.
     * @param  bool                      $overwrite Флаг, указывающий на возможность перезаписи существующего значения.
     * @throws \InvalidArgumentException Если URL некорректен.
     */
    public static function setCanonical(
        string $url = '',
        bool   $overwrite = true
    ): void
    {
        // Проверяем корректность URL с помощью фильтра.
        if (!filter_var($url, FILTER_VALIDATE_URL))
        {
            throw new \InvalidArgumentException('Некорректный URL для canonical.');
        }

        \xtetis\xengine\App::setParam('canonical', $url, $overwrite);
    }

    /**
     * Получает canonical URL для страницы.
     *
     * @return string Canonical URL.
     */
    public static function getCanonical(): string
    {
        return \xtetis\xengine\App::getParam('canonical', '');
    }

    /**
     * Устанавливает keywords для страницы.
     *
     * @param string $keywords  Ключевые слова.
     * @param bool   $overwrite Флаг, указывающий на возможность перезаписи.
     */
    public static function setKeywords(
        string $keywords = '',
        bool   $overwrite = true
    ): void
    {
        \xtetis\xengine\App::setParam('keywords', $keywords, $overwrite);
    }

    /**
     * Получает keywords для страницы.
     *
     * @return string Ключевые слова страницы.
     */
    public static function getKeywords(): string
    {
        return \xtetis\xengine\App::getParam('keywords', '');
    }

    /**
     * Очищает все SEO параметры страницы (Title, Description, Canonical, H1, Keywords).
     */
    public static function clearSeoParameters(): void
    {
        // Устанавливаем пустые значения для всех SEO параметров.
        \xtetis\xengine\App::setParam('title', '', true);
        \xtetis\xengine\App::setParam('description', '', true);
        \xtetis\xengine\App::setParam('canonical', '', true);
        \xtetis\xengine\App::setParam('h1', '', true);
        \xtetis\xengine\App::setParam('keywords', '', true);
    }

    /**
     * Устанавливает все мета-теги страницы (Title, Description, Canonical, Keywords) за один вызов.
     *
     * @param string $title       Заголовок страницы.
     * @param string $description Описание страницы.
     * @param string $canonical   URL страницы.
     * @param string $keywords    Ключевые слова страницы.
     * @param bool   $overwrite   Флаг, указывающий на возможность перезаписи существующих значений.
     */
    public static function setMetaTags(
        string $title = '',
        string $description = '',
        string $canonical = '',
        string $keywords = '',
        bool   $overwrite = true
    ): void
    {
        // Устанавливаем Title
        self::setTitle($title, $overwrite);

        // Устанавливаем Description
        self::setDescription($description, $overwrite);

        // Устанавливаем Canonical URL с проверкой корректности.
        if ('' !== $canonical)
        {
            self::setCanonical($canonical, $overwrite);
        }

        // Устанавливаем ключевые слова.
        self::setKeywords($keywords, $overwrite);
    }
}
