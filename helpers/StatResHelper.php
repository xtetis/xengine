<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для работы со статическими ресурсами (JS, CSS и т.д.).
 */
class StatResHelper
{
    /**
     * Добавляет JavaScript файл в шаблон.
     *
     * @param  string $filename            Полный путь к JS файлу.
     * @return bool   Возвращает true, если файл успешно добавлен, false в случае ошибки.
     */
    public static function addTemplateJs(
        string $filename = ''
    ): bool
    {
        // Проверка существования файла
        if (!file_exists($filename))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Статический файл ' . $filename . ' не существует');
        }

        // Проверка, является ли путь файлом
        if (!is_file($filename))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Статический файл ' . $filename . ' не является файлом');
        }

        // Извлечение расширения файла
        $path_parts = pathinfo($filename);
        $extension  = $path_parts['extension'];

        // Проверяем, является ли файл JavaScript
        if ('js' !== $extension)
        {
            return false;
        }

        $config_name        = 'static_template_js'; // Название параметра в конфигурации для хранения JS файлов
        $static_template_js = \xtetis\xengine\App::getParam($config_name, []);

        // Убедимся, что параметр является массивом
        if (!is_array($static_template_js))
        {
            $static_template_js = [];
        }

        // Генерация уникального имени файла на основе его содержимого (MD5-хеш)
        $basename     = basename($filename);
        $md5_file     = md5_file($filename);
        $new_filename = GENERATED_STAT_RES_DIRECTORY . '/' . $md5_file . '__' . $basename;

        // Если файл не существует в сгенерированной директории, копируем его
        if (!file_exists($new_filename))
        {
            file_put_contents($new_filename, file_get_contents($filename));
            @chmod($new_filename, 0777); // Устанавливаем права доступа
        }

        // Определяем относительный путь к файлу
        $relative_path = str_replace(dirname(ENGINE_DIRECTORY), '', $new_filename);

        // Если файл уже был добавлен ранее, возвращаем true
        if (in_array($relative_path, $static_template_js))
        {
            return true;
        }

        // Добавляем путь к файлу в массив статических ресурсов
        $static_template_js[] = $relative_path;

        // Сохраняем обновлённый список JS файлов
        \xtetis\xengine\App::setParam($config_name, $static_template_js);

        return true;
    }

    /**
     * Возвращает список добавленных JS файлов для шаблона.
     * Формирует HTML-код в виде тегов <script>.
     *
     * @return string Возвращает HTML код с подключенными JS файлами.
     */
    public static function getTemplateJsList(): string
    {
        $ret = "\n<!-- getTemplateJsList -->\n";

        $config_name        = 'static_template_js'; // Название параметра для хранения JS файлов
        $static_template_js = \xtetis\xengine\App::getParam($config_name, []);

        // Убедимся, что параметр является массивом
        if (!is_array($static_template_js))
        {
            $static_template_js = [];
        }

        // Формируем теги <script> для каждого добавленного JS файла
        foreach ($static_template_js as $js_file)
        {
            if (is_string($js_file))
            {
                $ret .= '<script src="' . $js_file . '"></script>' . "\n";
            }
        }

        $ret .= "<!-- /getTemplateJsList -->\n";

        return $ret;
    }

    /**
     * Добавляет CSS файл в шаблон.
     *
     * @param  string $filename            Полный путь к CSS файлу.
     * @return bool   Возвращает true, если файл успешно добавлен, false в случае ошибки.
     */
    public static function addTemplateCss(string $filename): bool
    {
        // Проверка существования файла
        if (!file_exists($filename))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('CSS файл ' . $filename . ' не существует');
        }

        // Проверка, является ли путь файлом
        if (!is_file($filename))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('CSS файл ' . $filename . ' не является файлом');
        }

        // Извлечение расширения файла
        $path_parts = pathinfo($filename);
        $extension  = $path_parts['extension'];

        // Проверяем, является ли файл CSS
        if ('css' !== $extension)
        {
            return false;
        }

        $config_name         = 'static_template_css'; // Название параметра в конфигурации для хранения CSS файлов
        $static_template_css = \xtetis\xengine\App::getParam($config_name, []);

        // Убедимся, что параметр является массивом
        if (!is_array($static_template_css))
        {
            $static_template_css = [];
        }

        // Генерация уникального имени файла на основе его содержимого (MD5-хеш)
        $basename     = basename($filename);
        $md5_file     = md5_file($filename);
        $new_filename = GENERATED_STAT_RES_DIRECTORY . '/' . $md5_file . '__' . $basename;

        // Если файл не существует в сгенерированной директории, копируем его
        if (!file_exists($new_filename))
        {
            file_put_contents($new_filename, file_get_contents($filename));
            @chmod($new_filename, 0777); // Устанавливаем права доступа
        }

        // Определяем относительный путь к файлу
        $relative_path = str_replace(dirname(ENGINE_DIRECTORY), '', $new_filename);

        // Если файл уже был добавлен ранее, возвращаем true
        if (in_array($relative_path, $static_template_css))
        {
            return true;
        }

        // Добавляем путь к файлу в массив статических ресурсов
        $static_template_css[] = $relative_path;

        // Сохраняем обновлённый список CSS файлов
        \xtetis\xengine\App::setParam($config_name, $static_template_css);

        return true;
    }

    /**
     * Возвращает список добавленных CSS файлов для шаблона.
     * Формирует HTML-код в виде тегов <link>.
     *
     * @return string Возвращает HTML код с подключенными CSS файлами.
     */
    public static function getTemplateCssList(): string
    {
        $ret = "\n<!-- getTemplateCssList -->\n";

        $config_name         = 'static_template_css'; // Название параметра для хранения CSS файлов
        $static_template_css = \xtetis\xengine\App::getParam($config_name, []);

        // Убедимся, что параметр является массивом
        if (!is_array($static_template_css))
        {
            $static_template_css = [];
        }

        // Формируем теги <link> для каждого добавленного CSS файла
        foreach ($static_template_css as $css_file)
        {
            if (is_string($css_file))
            {
                $ret .= '<link rel="stylesheet" href="' . $css_file . '" />' . "\n";
            }
        }

        $ret .= "<!-- /getTemplateCssList -->\n";

        return $ret;
    }

    /**
     * Очищает все статические ресурсы (JS и CSS).
     */
    public static function clearStaticResources(): void
    {
        self::clearStaticJs();
        self::clearStaticCss();
    }

    /**
     * Очищает список добавленных JS файлов.
     */
    public static function clearStaticJs(): void
    {
        \xtetis\xengine\App::setParam('static_template_js', []);
    }

    /**
     * Очищает список добавленных CSS файлов.
     */
    public static function clearStaticCss(): void
    {
        \xtetis\xengine\App::setParam('static_template_css', []);
    }
}
