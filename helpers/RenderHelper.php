<?php

namespace xtetis\xengine\helpers;

/**
 *
 */
class RenderHelper
{

    /**
     * Рендерим страницу как block + layer
     *
     * @param $block
     * @param $layout
     * @param array     $render_data
     */
    public static function renderPage(
        $block = 'page/index',
        $render_data = []
    )
    {
        $content = self::renderBlockCurrentComponent($block, $render_data);

        return self::renderLayout($content);

    }

    /**
     * Рендерим страницу как отрендереная страница + layer
     *
     * @param $block
     * @param $layout
     * @param array     $render_data
     */
    public static function renderLayout(
        $content = ''
    )
    {
        $layout = \xtetis\xengine\App::getLayout();

        return self::renderBlock(
            'layout/' . $layout,
            [
                'content' => $content,
            ]
        );

    }

    /**
     * @param $block
     * @param array $render_data
     */
    public static function renderBlockCurrentComponent(
        $block,
        $render_data = []
    )
    {
        return self::renderBlock(
            $block,
            $render_data,
            \xtetis\xengine\App::getApp()->getComponent()::getComponentClass()
        );
    }

    /**
     * Рендерим блок в папке Views
     *
     *          component_to_app - сначала в компоненте, потом в APP (компонентом может выступать и APP)
     *          app_to_component - сначала в APP, потом в компоненте (компонентом может выступать и APP)
     *          app - только в APP
     *          component - только в компонента (компонентом может выступать и APP)
     * @param $block                     -            имя блока
     * @param array                      $render_data - данные для гендеринга (массив)
     * @param $component_object_or_class -            где искать view
     */
    public static function renderBlock(
        $block,
        $render_data = [],
        $component_class = false
    )
    {
        // Если компонент не задан - то используем компонент APP
        if (!$component_class)
        {
            $component_root_directory = \xtetis\xengine\App::getApp()->getAppComponentClass()::getComponentDirectory();
        }
        // Иначе используем компонент указанный в параметре

        else
        {

            if (!\xtetis\xengine\models\Component::isChildOfComponent($component_class))
            {
                throw new \Exception('$component_class не является наследником компонента ' . (\xtetis\xengine\Config::isTest() ? print_r($component_class, true) : ''));
            }
            $component_root_directory = $component_class::getComponentDirectory();
        }

        $filename_view = $component_root_directory . '/views/' . $block . '.php';

        if (file_exists($filename_view))
        {
            return self::renderFile(
                $filename_view,
                $render_data
            );
        }
        else
        {
            if (ob_get_length() > 0)
            {
                ob_clean();
            }
            echo '<!-- ' . "\n";
            debug_print_backtrace();
            echo "\n" . ' -->' . "\n";
            throw new \Exception('Блок ' . $block . ' не найден ' . (\xtetis\xengine\Config::isTest() ? $filename_view : ''));
        }

    }

    /**
     * Рендерим файл
     */
    public static function renderFile(
        $filename,
        $render_data = []
    )
    {
        // Время начала работы запроса
        $start_microtime = microtime(true);

        extract($render_data);
        $fileView = $filename;
        if (file_exists($fileView))
        {

            $base_filename = dirname(realpath($filename));
            $base_filename = str_replace(ENGINE_DIRECTORY, '', $base_filename);
            $base_filename = $base_filename . '/' . basename($filename, '.php');
            ob_start();
            echo "\n<!-- start render file " . $base_filename . " -->\n";
            include $fileView;
            echo "\n<!-- end render file " . $base_filename . " time:".(microtime(true) - $start_microtime)." -->\n";
            $render_block = ob_get_contents();
            ob_end_clean();
        }
        else
        {
            ob_clean();
            echo '<!-- ' . "\n";
            debug_print_backtrace();
            echo "\n" . ' -->' . "\n";
            \xtetis\xengine\helpers\LogHelper::customDie('Файл не найден <!-- ' . $fileView . ' -->');
        }

        return $render_block;
    }
}
