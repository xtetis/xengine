<?php

namespace xtetis\xengine\helpers;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

use \xtetis\xengine\exception\ParseException;
use \xtetis\xengine\exception\NoticeException;
use \xtetis\xengine\exception\StrictException;
use \xtetis\xengine\exception\WarningException;
use \xtetis\xengine\exception\CoreErrorException;
use \xtetis\xengine\exception\UserErrorException;
use \xtetis\xengine\exception\DeprecatedException;
use \xtetis\xengine\exception\UserNoticeException;
use \xtetis\xengine\exception\CoreWarningException;
use \xtetis\xengine\exception\UserWarningException;
use \xtetis\xengine\exception\CompileErrorException;
use \xtetis\xengine\exception\CompileWarningException;
use \xtetis\xengine\exception\UserDeprecatedException;
use \xtetis\xengine\exception\RecoverableErrorException;

/**
 * Глобальная переменная для обработки ошибок
 */
global $model_error_handler;

/**
 * Класс для работы с ошибками и исключениями
 */
class ErrorHelper
{
    /**
     * Номер ошибки
     */
    public $errno;

    /**
     * Текст ошибки
     */
    public $errstr;

    /**
     * Файл, в котором произошла ошибка
     */
    public $errfile;

    /**
     * Строка, на которой произошла ошибка
     */
    public $errline;

    /**
     * Исключение, которое будет обрабатываться
     */
    public \Throwable $exception;

    /**
     * Имя файла для логирования ошибок
     */
    public $log_error_filename = '';

    /**
     * Имя файла для логирования исключений
     */
    public $log_exception_filename = '';

    /**
     * Массив для хранения данных об ошибке
     */
    public array $log_error_array = [];

    /**
     * Массив для хранения данных об исключении
     */
    public array $log_exception_array = [];

    /**
     * Краткое имя класса исключения
     */
    public $short_class_exception_name = '';

    /**
     * Исключения, которые должны быть отрендерены в текущем макете
     */
    public $exception_render_in_layout = [
        'UserErrorException',
        'UserWarningException',
        'UserNoticeException',
        'WarningException',
    ];



    /**
     * Тип исключений, для которых в случает AJAX ответа отправлять расширенную информацию
     * (не только текст ошибки) 
     */
    public $exception_ajax_extended_data = [
        'WarningException',
    ];

    /**
     * Если код ошибки специально не установлен с помощью
     * ErrorException(СООБЩЕНИЕ_ОБ_ОШИБКЕ, КОД_ОШИБКИ .... то 
     * по-умолчанию устанавливаем следующие коды
     */
    public $default_http_codes = [
        'ErrorException'            => 500,
        'ParseException'            => 500,
        'NoticeException'           => 200,
        'StrictException'           => 500,
        'WarningException'          => 200,
        'CoreErrorException'        => 500,
        'UserErrorException'        => 200,
        'DeprecatedException'       => 500,
        'UserNoticeException'       => 200,
        'CoreWarningException'      => 500,
        'UserWarningException'      => 200,
        'CompileErrorException'     => 500,
        'CompileWarningException'   => 500,
        'UserDeprecatedException'   => 200,
        'RecoverableErrorException' => 500,
    ];

    /**
     * Максимальное количество записей об ошибках
     */
    public $max_log_error_count = 100;

    /**
     * Список всех возможных типов ошибок и их соответствие кодам
     */
    public $exceptions = [
        E_ERROR             => 'E_ERROR',
        E_WARNING           => 'E_WARNING',
        E_PARSE             => 'E_PARSE',
        E_NOTICE            => 'E_NOTICE',
        E_CORE_ERROR        => 'E_CORE_ERROR',
        E_CORE_WARNING      => 'E_CORE_WARNING',
        E_COMPILE_ERROR     => 'E_COMPILE_ERROR',
        E_COMPILE_WARNING   => 'E_COMPILE_WARNING',
        E_USER_ERROR        => 'E_USER_ERROR',
        E_USER_WARNING      => 'E_USER_WARNING',
        E_USER_NOTICE       => 'E_USER_NOTICE',
        E_STRICT            => 'E_STRICT',
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
        E_DEPRECATED        => 'E_DEPRECATED',
        E_USER_DEPRECATED   => 'E_USER_DEPRECATED',
        E_ALL               => 'E_ALL',
    ];

    /**
     * Возвращает имя текущей ошибки на основе её кода
     *
     * @return string
     */
    public function getCurrentErrorName(): string
    {
        return $this->exceptions[$this->errno] ?? '';
    }

    /**
     * Основной обработчик ошибок
     *
     * @param int    $errno   Код ошибки
     * @param string $errstr  Сообщение об ошибке
     * @param string $errfile Файл, в котором произошла ошибка
     * @param int    $errline Строка, на которой произошла ошибка
     */
    public static function errorHandler(
        $errno,
        $errstr,
        $errfile,
        $errline
    )
    {

        $model_error_handler          = new self();
        $model_error_handler->errno   = $errno;
        $model_error_handler->errstr  = $errstr;
        $model_error_handler->errfile = $errfile;
        $model_error_handler->errline = $errline;
        $model_error_handler->prepareError();

        // Возвращает false, чтобы позволить PHP продолжить обработку ошибок

        return false;
    }

    /**
     * Обработка ошибки в зависимости от её типа
     */
    public function prepareError()
    {
        // Если каким-то компонентом был изменен уровень ошибок - устанавливаем
        // его в тот, что в константе ERROR_LEVEL_ARRAY
        error_reporting(
            array_sum(
                array_keys(ERROR_LEVEL_ARRAY, true)
            )
        );

        // Если ошибка пропущена (например, подавлена с помощью '@'), не обрабатываем её
        if ($this->skipError())
        {
            return;
        }

        // Логируем ошибку
        $this->logError();

        // В зависимости от типа ошибки выбрасываем соответствующее исключение
        switch ($this->errno)
        {
            case E_ERROR: throw new \ErrorException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_WARNING: throw new WarningException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_PARSE: throw new ParseException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_NOTICE: throw new NoticeException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_CORE_ERROR: throw new CoreErrorException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_CORE_WARNING: throw new CoreWarningException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_COMPILE_ERROR: throw new CompileErrorException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_COMPILE_WARNING: throw new CompileWarningException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_USER_ERROR: throw new UserErrorException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_USER_WARNING: throw new UserWarningException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_USER_NOTICE: throw new UserNoticeException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_STRICT: throw new StrictException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_RECOVERABLE_ERROR: throw new RecoverableErrorException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_DEPRECATED: throw new DeprecatedException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
            case E_USER_DEPRECATED: throw new UserDeprecatedException($this->errstr, 0, $this->errno, $this->errfile, $this->errline);
        }
    }

    /**
     * Основной обработчик исключений
     *
     * @param \Throwable $exception Исключение для обработки
     */
    public static function exceptionHandler(\Throwable $exception)
    {
        $model_error_handler            = new self();
        $model_error_handler->exception = $exception;
        $model_error_handler->prepareException();
    }

    /**
     * Проверка на необходимость пропуска ошибки
     *
     * @return bool
     */
    public function skipError(): bool
    {
        $current_error_level = error_reporting();

        foreach ($this->exceptions as $error_number => $value)
        {
            if ($error_number == $this->errno)
            {
                // Текущая ошибка включена с помощью error_reporting
                if (($current_error_level & $error_number) == $error_number)
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Возвращает массив с уровнем ошибок
     *
     * @param  $error_level
     * @return mixed
     */
    public static function getErrorLevelArray($error_level = null)
    {

        if (null == $error_level)
        {
            $error_level = error_reporting();
        }

        $model = new self();

        $result = [];
        foreach ($model->exceptions as $number => $name)
        {
            if (($error_level & $number) == $number)
            {
                $result[$number] = $name;
            }
        }

        return $result;
    }

    /**
     * Проверка на необходимость пропуска исключения
     *
     * @return bool
     */
    public function skipException(): bool
    {
        return false;
    }

    /**
     * Подготовка исключения к обработке
     */
    public function prepareException()
    {
        // Получение короткого имени класса исключения с помощью ReflectionClass
        $reflect                          = new \ReflectionClass($this->exception);
        $this->short_class_exception_name = $reflect->getShortName(); // Короткое имя класса


        if ($this->skipException())
        {
            return;
        }

        // Логируем исключение
        $this->logException();

        // Отображаем исключение пользователю
        $this->renderException();
    }

    /**
     * Логирует информацию об ошибке в файл
     *
     * @return bool Возвращает true после успешного логирования
     */
    public function logError()
    {
        // Определение имени файла для логирования ошибки.
        // Формируется путь к файлу с названием, соответствующим типу ошибки
        $this->log_error_filename = ERRORS_DIRECTORY .
        '/error.' .
        strtolower($this->getCurrentErrorName()) . // Имя ошибки (например, E_ERROR) приводится к нижнему регистру
        '.json';

        // Формирование массива данных об ошибке
        $this->log_error_array = [
            'request_date' => date('d.m.Y H:i:s'),    // Дата и время ошибки
            'url_request' => $_SERVER['REQUEST_URI'], // URL запроса, где произошла ошибка
            'errno' => $this->errno,                  // Код ошибки
            'errstr' => $this->errstr,                // Описание ошибки
            'errfile' => $this->errfile,              // Файл, где произошла ошибка
            'errline' => $this->errline,              // Строка, на которой произошла ошибка
        ];

        // Логирование массива ошибок в JSON-файл с помощью LogHelper
        \xtetis\xengine\helpers\LogHelper::logArrayToJsonFile(
            $this->log_error_filename, // Имя файла
            $this->log_error_array     // Массив с данными ошибки
        );

        // Возвращаем true после успешного логирования

        return true;
    }

    /**
     * Логирует информацию об исключении в файл
     *
     * @return bool Возвращает true после успешного логирования
     */
    public function logException()
    {

        // Определение имени файла для логирования исключения.
        // Формируется путь к файлу с названием, соответствующим типу исключения
        $this->log_exception_filename = ERRORS_DIRECTORY .
        '/exception.' .
        strtolower($this->short_class_exception_name) . // Приведение имени класса к нижнему регистру
        '.json';

        // Формирование массива данных об исключении
        $this->log_exception_array = [
            'request_date' => date('d.m.Y H:i:s'),          // Дата и время исключения
            'url_request' => $_SERVER['REQUEST_URI'],       // URL запроса, где произошло исключение
            'exception' => print_r($this->exception, true), // Полная информация об исключении
        ];

        // Логирование массива исключений в JSON-файл с помощью LogHelper
        \xtetis\xengine\helpers\LogHelper::logArrayToJsonFile(
            $this->log_exception_filename, // Имя файла
            $this->log_exception_array     // Массив с данными исключения
        );

        // Возвращаем true после успешного логирования

        return true;
    }

    /**
     * Вывод ошибки (исключения)
     */
    public function renderException()
    {
        // Устанавливаем соответствующие HTTP заголовки
        $this->setHttpHeaders();

        // Проверяем, нужно ли выводить исключение в общем оформлении (шаблоне)
        if (in_array($this->short_class_exception_name, $this->exception_render_in_layout))
        {
            // Если запрос пришел через AJAX
            if (\xtetis\xengine\helpers\RequestHelper::isAjax())
            {
                // Безопасно очищаем буфер вывода
                $this->cleanOutputBuffer();

                // Тип исключений, для которых в случает AJAX ответа отправлять расширенную информацию (не только текст ошибки)
                if (in_array($this->short_class_exception_name, $this->exception_ajax_extended_data))
                {
                    // Отправляем JSON-ответ
                    $this->formatAjaxResponse([
                        'result' => false,
                        'errors' => [
                            'common' => [
                                $this->short_class_exception_name.
                                ': '.$this->exception->getMessage().
                                ' File: '.$this->exception->getFile().
                                ':'.$this->exception->getLine()
                            ],
                        ],
                    ]);
                }
                else
                {
                    // Отправляем JSON-ответ
                    $this->formatAjaxResponse([
                        'result' => false,
                        'errors' => [
                            'common' => [$this->exception->getMessage()],
                        ],
                    ]);
                }
            }
            else
            {
                // Безопасно очищаем буфер вывода
                $this->cleanOutputBuffer();
                
                // Если запрос не AJAX, рендерим страницу с исключением
                $content = \xtetis\xengine\helpers\RenderHelper::renderFile(
                    ERROR_USER_PAGE, // Путь к файлу страницы ошибки
                    [
                        'exception' => $this->exception, // Передаем данные исключения в шаблон
                    ]
                );
                // Выводим основной шаблон с контентом ошибки
                echo \xtetis\xengine\helpers\RenderHelper::renderLayout($content);
            }
        }
        else
        {

            // Безопасно очищаем буфер вывода
            $this->cleanOutputBuffer();

            // Выводим стандартный шаблон для ошибки
            $exception = $this->exception;
            include ERROR_LAYOUT_FILENAME;
        }

        // Завершаем выполнение скрипта
        exit;
    }

    /**
     * Устанавливает HTTP-заголовки в зависимости от кода исключения
     */
    private function setHttpHeaders()
    {
        $current_exception_code = $this->exception->getCode();

        // Если исключение отправлено с помощью errorHandler
        // и код ошибки 0
        if (!$current_exception_code)
        {
            if (isset($this->default_http_codes[$this->short_class_exception_name]))
            {
                $current_exception_code = $this->default_http_codes[$this->short_class_exception_name];
            }

            if (defined('ERROR_HANDLER_DEFAULT_HTTP_CODES'))
            {
                if (is_array(ERROR_HANDLER_DEFAULT_HTTP_CODES))
                {
                    foreach (ERROR_HANDLER_DEFAULT_HTTP_CODES as $class_name => $http_code) 
                    {
                        if ($this->short_class_exception_name == $class_name)
                        {
                            if (is_integer($http_code))
                            {
                                $current_exception_code = $http_code;
                            }
                        }
                    }
                }
            }
        }
        

        switch ($current_exception_code)
        {
            case 404:
                header('HTTP/1.0 404 Not Found');
                break;
            case 200:
                break;
            case 500:
            default:
                header('HTTP/1.1 500 Internal Server Error');
                break;
        }
    }

    /**
     * Форматирует и отправляет JSON-ответ
     */
    private function formatAjaxResponse(array $response)
    {
        echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    /**
     * Безопасно очищает буфер вывода
     */
    private function cleanOutputBuffer()
    {
        if (ob_get_level() > 0)
        {
            ob_end_clean();
        }
    }
}
