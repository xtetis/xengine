<?php

namespace xtetis\xengine\helpers;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 * Хелпер для работы с JSON
 */
class JsonHelper
{

    /**
     * Конвертирует массив в JSON с оговоркой, если предопределена константа JSON_UNESCAPED_UNICODE
     *
     * @param  array   $array
     * @return mixed
     */
    public static function arrayToJson(array $array = [])
    {
        if (defined('JSON_UNESCAPED_UNICODE'))
        {
            $ret = json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        else
        {
            $ret = json_encode($array);
        }

        return $ret;
    }

    /**
     * Проверяет является ли строка корректным JSON
     * 
     * @param string $string
     */
    public static function isJson(string $string): bool
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

}
