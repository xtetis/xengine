<?php
namespace xtetis\xengine\helpers;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

global $xengine_pdo_connect;
global $xengine_pg_connect;

/**
 *
 */
class DbConnectHelper
{
    /**
     * Возвращает коннект к онлайну
     */
    public static function getConnect($connect_name = '')
    {
        return self::getConnectPdo($connect_name);
    }

    /**
     * Возвращает коннект через ПДО
     */
    public static function getConnectPdo($connect_name = '')
    {
        global $xengine_pdo_connect;

        if (!isset($xengine_pdo_connect[$connect_name]))
        {
            $db_connections = \xtetis\xengine\helpers\ConfigHelper::getConfigArray('params.db_connections');

            if (!isset($db_connections[$connect_name]['host']))
            {
                die('Не указан host для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['port']))
            {
                die('Не указан port для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['dbname']))
            {
                die('Не указан dbname для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['user']))
            {
                die('Не указан user для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['password']))
            {
                die('Не указан password для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['type']))
            {
                die('Не указан type для БД ' . $connect_name);
            }

            $dsn = $db_connections[$connect_name]['type'] .
                ':dbname=' . $db_connections[$connect_name]['dbname'] .
                ';port=' . $db_connections[$connect_name]['port'] .
                ';host=' . $db_connections[$connect_name]['host'];
            $user     = $db_connections[$connect_name]['user'];
            $password = $db_connections[$connect_name]['password'];

            try
            {
                $dbh                                = new \PDO($dsn, $user, $password);
                $xengine_pdo_connect[$connect_name] = $dbh;
            }
            catch (\PDOException $e)
            {
                die($e->getMessage());
            }
        }

        return $xengine_pdo_connect[$connect_name];

    }

    /**
     * Возвращает коннект с помощью pg_connect
     */
    public static function getConnectPg($connect_name = '')
    {
        global $xengine_pg_connect;

        if (!isset($xengine_pg_connect[$connect_name]))
        {
            $db_connections = \xtetis\xengine\helpers\ConfigHelper::getConfigArray('params.db_connections');

            if (!isset($db_connections[$connect_name]['host']))
            {
                die('Не указан host для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['port']))
            {
                die('Не указан port для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['dbname']))
            {
                die('Не указан dbname для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['user']))
            {
                die('Не указан user для БД ' . $connect_name);
            }
            if (!isset($db_connections[$connect_name]['password']))
            {
                die('Не указан password для БД ' . $connect_name);
            }

            $connect_str = ' host=' . $db_connections[$connect_name]['host'] .
                ' port=' . $db_connections[$connect_name]['port'] .
                ' dbname=' . $db_connections[$connect_name]['dbname'] .
                ' user=' . $db_connections[$connect_name]['user'] .
                ' password=' . $db_connections[$connect_name]['password'] . '';

            try
            {
                $xengine_pg_connect[$connect_name] = pg_connect($connect_str) or die('Не удалось подключиться к БД ' . $connect_name);
            }
            catch (\Exception $e)
            {
                die($e->getMessage());
            }
        }

        return $xengine_pg_connect[$connect_name];

    }

}
