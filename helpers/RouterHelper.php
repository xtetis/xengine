<?php

namespace xtetis\xengine\helpers;


class RouterHelper
{
    /**
     * массив для хранения соответствия url => функция
     * @var array
     */
    private static $routes = [];

    // запрещаем создание и копирование статического объекта
    private function __construct()
    {
    }
    
    private function __clone()
    {
    }

    /**
     * данный метод принимает шаблон url-адреса
     * как шаблон регулярного выражения и связывает его
     * с пользовательской функцией
     * 
     * @param $pattern
     * @param $callback
     */
    public static function route(
        $pattern,
        $callback
    )
    {
        // функция str_replace здесь нужна, для экранирования всех прямых слешей
        // так как они используются в качестве маркеров регулярного выражения
        //$pattern                = '/^' . str_replace('/', '\/', $pattern) . '$/';
        self::$routes[$pattern] = $callback;
    }

    /**
     *  Метод проверяет запрошенный $url(адрес) на
     *  соответствие адресам, хранящимся в массиве $routes
     */
    public static function execute($url)
    {

        if (\xtetis\xengine\App::getApp()->runAdditionalSefHandler($_SERVER['REQUEST_URI']))
        {
            return true;
        }

        foreach (self::$routes as $pattern => $callback)
        {
            if (preg_match($pattern, $url, $params)) // сравнение идет через регулярное выражение
            {
                // соответствие найдено, поэтому удаляем первый элемент из массива $params
                // который содержит всю найденную строку
                array_shift($params);
                return call_user_func_array($callback, [$params]);
            }
        }
    }
}
