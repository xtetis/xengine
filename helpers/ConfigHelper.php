<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для работы с конфигурационными файлами
 */
class ConfigHelper
{
    /**
     * Возвращает массив из конфигурационного файла.
     *
     * @param  string $config_name         Имя конфигурационного файла (без расширения).
     * @return array  Возвращает массив конфигурации или пустой массив, если файл не найден.
     */
    public static function getConfigArray(string $config_name): array {
        $ret = [];

        // Формируем путь к файлу конфигурации
        $filename = ENGINE_DIRECTORY . '/config/' . $config_name . '.php';

        // Проверяем существование файла и загружаем его, если файл найден
        if (file_exists($filename))
        {
            $ret = require $filename;
        }

        return $ret;
    }

    /**
     * Возвращает значение из конфигурационного файла по ключу.
     *
     * @param  string $config_name         Имя конфигурационного файла (без расширения).
     * @param  string $key                 Ключ конфигурации.
     * @param  mixed  $default             Значение по умолчанию, если ключ не найден.
     * @return mixed  Возвращает значение конфигурации или значение по умолчанию, если ключ отсутствует.
     */
    public static function getConfigValue(
        string $config_name,
        string $key,
               $default = null
    )
    {
        // Получаем массив конфигурации
        $config = self::getConfigArray($config_name);

        // Возвращаем значение по ключу или значение по умолчанию, если ключ не существует

        return $config[$key] ?? $default;
    }

    /**
     * Проверяет наличие ключа в конфигурационном файле.
     *
     * @param  string $config_name         Имя конфигурационного файла (без расширения).
     * @param  string $key                 Ключ конфигурации.
     * @return bool   Возвращает true, если ключ существует, иначе false.
     */
    public static function hasConfigKey(
        string $config_name,
        string $key
    ): bool
    {
        // Получаем массив конфигурации
        $config = self::getConfigArray($config_name);

        // Проверяем наличие ключа

        return array_key_exists($key, $config);
    }

    /**
     * Обновляет значение конфигурационного файла (перезаписывает файл).
     *
     * @param  string $config_name         Имя конфигурационного файла (без расширения).
     * @param  string $key                 Ключ для обновления.
     * @param  mixed  $value               Новое значение.
     * @return bool   Возвращает true, если файл был успешно обновлен.
     */
    public static function setConfigValue(
        string $config_name,
        string $key,
               $value
    ): bool
    {
        // Получаем существующую конфигурацию
        $config = self::getConfigArray($config_name);

        // Обновляем значение ключа
        $config[$key] = $value;

        // Формируем путь к файлу конфигурации
        $filename = ENGINE_DIRECTORY . '/config/' . $config_name . '.php';

        // Перезаписываем файл с обновленной конфигурацией
        $exportedConfig = '<?php return ' . var_export($config, true) . ';';

        // Проверяем успешность записи в файл

        return (bool) file_put_contents($filename, $exportedConfig);
    }

    /**
     * Удаляет ключ из конфигурационного файла.
     *
     * @param  string $config_name         Имя конфигурационного файла (без расширения).
     * @param  string $key                 Ключ для удаления.
     * @return bool   Возвращает true, если ключ был удален и файл обновлен.
     */
    public static function removeConfigKey(
        string $config_name,
        string $key
    ): bool
    {
        // Получаем конфигурацию
        $config = self::getConfigArray($config_name);

        // Проверяем, существует ли ключ
        if (array_key_exists($key, $config))
        {
            // Удаляем ключ
            unset($config[$key]);

            // Перезаписываем файл конфигурации
            $filename       = ENGINE_DIRECTORY . '/config/' . $config_name . '.php';
            $exportedConfig = '<?php return ' . var_export($config, true) . ';';

            // Записываем изменения в файл

            return (bool) file_put_contents($filename, $exportedConfig);
        }

        return false;
    }
}
