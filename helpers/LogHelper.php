<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для логирования и вывода ошибок
 */
class LogHelper
{

    /**
     * @param $message
     */
    public static function customDie(
        $message = ''
    )
    {
        $error_data = "\n\n\n\n\n==========================================================\n";
        $error_data .= date('Y-m-d H:i:s') . "\n";
        $error_data .= "==========================================================\n";
        $error_data .= $message . "\n";
        $error_data .= "\n==========================================================\n";

        @file_put_contents(ERRORS_DIRECTORY . '/custom_die.txt', $error_data, FILE_APPEND);
        @chmod(ERRORS_DIRECTORY . '/custom_die.txt', 0777);

        if (ob_get_contents())
        {
            ob_end_clean();
        }

        flush();

        if (ob_get_length() > 0)
        {
            ob_clean();
        }

        if (!defined('CUSTOM_DIE'))
        {
            die($message);
        }

        $custom_die_method = CUSTOM_DIE;
        if (!is_string($custom_die_method))
        {
            die($message);
        }

        if (!is_callable($custom_die_method))
        {
            die($message);
        }

        $class_string_arr = explode(':', $custom_die_method);
        $class_string     = $class_string_arr[0];

        $parent_class_name = get_parent_class($class_string);

        if ('xtetis\xengine\models\Component' != $parent_class_name)
        {
            die($message);
        }

        call_user_func_array(
            $custom_die_method,
            [
                $message,
            ]
        );

        die($message);
    }

    /**
     * Логирование данных в файл JSON
     */
    public static function logArrayToJsonFile(
        string $filename = '',
        array  $log_item_array = [],
        int    $max_log_count = 100
    )
    {
        $dir = dirname($filename);

        if (!is_writable($dir))
        {
            throw new \ErrorException('Директория ' . $dir . ' не доступна для записи');
        }

        if (file_exists($filename))
        {
            if (!is_writable($filename))
            {
                throw new \ErrorException('Файл ' . $filename . ' не доступен для записи');
            }
        }
        else
        {
            $json_empty_array = json_encode([], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            if (false === file_put_contents($filename, $json_empty_array))
            {
                throw new \ErrorException('В файл списка логов ' . $filename . ' не удалось записать данные');
            }

            if (!chmod($filename, 0777))
            {
                throw new \ErrorException('Для файла логов ' . $filename . ' не удалось сменить права на 777');
            }
        }

        if (!is_readable($filename))
        {
            throw new \ErrorException('Файл логов ' . $filename . ' не доступен для чтения');
        }

        $log_filedata = file_get_contents($filename);
        $log_array    = json_decode($log_filedata, true);

        if (!is_array($log_array))
        {
            $log_array = [];
        }

        $log_array[] = $log_item_array;

        if (count($log_array) > $max_log_count)
        {
            $log_array = array_reverse($log_array);
            $arr       = array_chunk($log_array, $max_log_count);
            $log_array = array_reverse($arr[0]);
        }

        if (!file_put_contents(
            $filename,
            json_encode($log_array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))
        )
        {
            throw new \ErrorException('Не удалось записать данные в лог ' . $filename . '');
        }

        return true;

    }
}
