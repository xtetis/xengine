<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для работы с запросами через cURL
 */
class CurlHelper
{
    /**
     * Выполняет HTTP-запрос на указанный URL.
     * Возвращает массив с информацией о запросе, включая текст ответа и HTTP-код.
     *
     * @param  string $url               URL для выполнения запроса.
     * @param  array  $data              (Необязательно) Данные для отправки POST-запросом.
     * @param  array  $header            (Необязательно) Заголовки для HTTP-запроса.
     * @return array  Результат запроса, включая ответ и информацию cURL.
     */
    public static function getRequest(
        string $url,
        array  $data = [],
        array  $header = []
    ): array {
        // Инициализация cURL
        $ch = curl_init();

        // Установка URL
        curl_setopt($ch, CURLOPT_URL, $url);

        // Если переданы данные, выполняем POST-запрос
        if (!empty($data))
        {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // Преобразуем данные в строку для передачи
        }

        // Если переданы заголовки, добавляем их в запрос
        if (!empty($header))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

                                                        // Устанавливаем опции для возврата результата и ограничения времени выполнения
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Возвращает результат как строку
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);    // Таймаут подключения
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);          // Общий таймаут запроса

        // Инициализация переменных для хранения результатов
        $result = [
            'result' => false,  // Статус успеха запроса
            'curl_info' => [],  // Информация cURL
            'response' => null, // Текст ответа
            'http_code' => 0,   // HTTP-код ответа
            'last_error' => '', // Последняя ошибка (если возникла)
        ];

        try {
            // Выполнение запроса
            $response = curl_exec($ch);

            // Если запрос не удался, записываем ошибку
            if (false === $response)
            {
                $result['last_error'] = curl_error($ch);
            }
            else
            {
                // Запрос успешен, получаем информацию о cURL и HTTP-код
                $result['curl_info'] = curl_getinfo($ch);
                $result['http_code'] = $result['curl_info']['http_code'] ?? 0;
                $result['response']  = $response;

                // Если HTTP-код 200, считаем запрос успешным
                if (200 === $result['http_code'])
                {
                    $result['result'] = true;
                }
            }
        }
        catch (\Exception $e)
        {
            // Ловим исключения и записываем их сообщение в ошибки
            $result['last_error'] = 'Ошибка: ' . $e->getMessage();
        }
        finally
        {
            // Закрываем соединение cURL
            curl_close($ch);
        }

        return $result;
    }
}
