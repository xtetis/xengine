<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для работы с урлами.
 */
class UrlHelper
{
    /**
     * Возвращает протокол для генерации URL: http или https.
     *
     * @return string Протокол ('http' или 'https').
     */
    public static function getProtocol(): string
    {
        // Проверяем, определена ли константа 'PROTOCOL'
        if (defined('PROTOCOL'))
        {
            // Проверяем, допустим ли протокол, иначе возвращаем 'http'
            return in_array(PROTOCOL, ['http', 'https']) ? PROTOCOL : 'http';
        }
        // Если константа не определена, возвращаем 'http' по умолчанию

        return 'http';
    }

    /**
     * Генерирует URL на основе заданных параметров.
     *
     *                      - 'path' (array): части пути URL;
     *                      - 'query' (array): параметры запроса (query string);
     *                      - 'with_host' (bool): добавлять ли хост в URL.
     * @param  array       $params                      Ассоциативный массив с ключами:
     * @param  string|bool $component_class             Класс компонента для добавления в URL, если требуется.
     * @return string      Сформированный URL.
     */
    public static function makeUrl(
        array $params = [],
              $component_class = false
    ): string
    {
        // Проверяем компонент, если не задан — используем пустую строку
        $component_url = $component_class ? self::getComponentUrlSafe($component_class) : '';

        // Формируем базовый хост
        $host = isset($params['with_host']) && $params['with_host']
        ? self::getProtocol() . '://' . $_SERVER['HTTP_HOST'] . '/'
        : '/';

        // Формируем части пути URL
        $path = $params['path'] ?? [];
        $path = is_array($path) ? $path : [];

        // Добавляем компонент URL в начало пути, если он задан
        if ($component_url)
        {
            array_unshift($path, $component_url);
        }

        // Генерируем строку пути
        $path_string = implode('/', $path);

        // Генерируем строку запроса (query string)
        $query        = $params['query'] ?? [];
        $query_string = $query ? http_build_query($query) : '';

        // Возвращаем URL в зависимости от настройки SEF (человеко-понятный URL)

        return SEF
        ? $host . $path_string . ($query_string ? '?' . $query_string : '')
        : $host . '?_path=' . $path_string . ($query_string ? '&' . $query_string : '');
    }

    /**
     * Генерация URL для текущего компонента.
     *
     * @param  array  $params                      Параметры, аналогичные методу makeUrl().
     * @return string Сформированный URL.
     */
    public static function makeUrlCurrentComponent(array $params = []): string
    {
        // Получаем текущий компонент приложения и вызываем makeUrl
        return self::makeUrl($params, \xtetis\xengine\App::getApp()->getComponent());
    }

    /**
     * Возвращает URL для указанного компонента, если это допустимый компонент.
     *
     * @param  string     $component_class Класс компонента.
     * @throws \Exception Если класс компонента не является наследником компонента.
     * @return string     URL компонента.
     */
    private static function getComponentUrlSafe(string $component_class): string
    {
        if (!\xtetis\xengine\models\Component::isChildOfComponent($component_class))
        {
            throw new \Exception(
                sprintf(
                    '$component_class не является наследником компонента %s',
                    (\xtetis\xengine\Config::isTest() ? print_r($component_class, true) : '')
                )
            );
        }

        return $component_class::getComponentUrl();
    }

    /**
     * Проверяет валидность URL.
     *
     * @param  string $url  URL для проверки.
     * @return bool   true, если URL валиден, иначе false.
     */
    public static function isValidUrl(string $url): bool
    {
        return filter_var($url, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * Добавляет или заменяет параметры в URL.
     *
     * @param  string $url                             URL для модификации.
     * @param  array  $new_params                      Параметры для добавления или замены.
     * @return string Модифицированный URL.
     */
    public static function addOrReplaceParams(
        string $url,
        array  $new_params
    ): string
    {
        $parsed_url = parse_url($url);
        parse_str($parsed_url['query'] ?? '', $query);
        $query        = array_merge($query, $new_params);
        $query_string = http_build_query($query);

        return (isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '') .
            ($parsed_url['host'] ?? '') .
            ($parsed_url['path'] ?? '') .
            '?' . $query_string;
    }

}
