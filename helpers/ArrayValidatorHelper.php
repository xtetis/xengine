<?php

namespace xtetis\xengine\helpers;

/**
 *
 */
class ArrayValidatorHelper
{

    /**
     * @var mixed
     * [
     *      [
     *          'fields' = ['field1','field20,...]
     *          'action' = 'string'
     *          'params' = [
     *              'max' => 1,
     *              'message' => '',
     *          ],
     *      ],
     *      ...
     * ]
     */
    public $rules;

    /**
     * @var array
     */
    protected $labels = [
        'snils' => 'СНИЛС',
    ];

    /**
     * @var array
     */
    protected $validateErrorMessages = [
        'Required'        => 'Поле "%field%" не задано',
        'StringMinLength' => 'Минимальная длина поля "%field%" составляет %min% символов',
        'StringMaxLength' => 'Максимальная длина поля "%field%" составляет %max% символов',
        'RegexTest'       => 'Поле "%field%" имеет некорректный формат',
        'Snils'           => 'Поле "%field%" не является корректным СНИЛСом',
    ];

    /**
     * @var mixed
     */
    public $data;

    /**
     * @var mixed
     */
    protected $current_rule;
    /**
     * @var mixed
     */
    protected $current_action;
    /**
     * @var mixed
     */
    protected $currentErrorMessage;
    /**
     * @var mixed
     */
    protected $currentFieldLabel;

    /**
     * @var mixed
     */
    public $errors = [];

    public function __construct()
    {
        $this->errors['result'] = true;
    }

    /**
     * Метод устанавливает правила валидации
     *
     * @param  $rules
     * @return mixed
     */
    public function setRules($rules)
    {
        return $this->rules = $rules;
    }

    /**
     * @param  $data
     * @return mixed
     */
    public function setData($data)
    {
        return $this->data = $data;
    }

    /**
     * @param $field
     * @param $name
     */
    public function setLabel(
        $field,
        $name
    )
    {
        $this->labels[$field] = $name;
    }

    /**
     * @return mixed
     */
    public function checkRules()
    {
        if ($this->errors['result'])
        {
            foreach ($this->rules as $rule)
            {
                if (
                    (!isset($rule['fields'])) ||
                    (!isset($rule['action'])) ||
                    (!isset($rule['params'])) ||
                    (!is_array($rule['fields'])) ||
                    (!count($rule['fields'])) ||
                    (!is_array($rule['params'])) ||
                    (!strlen($rule['action']))

                )
                {
                    $this->errors['result'] = false;
                    $this->errors['common'] = 'Правила валидации некорректные. Не заданы параметры.';
                    break;
                }
            }
        }

        if ($this->errors['result'])
        {
            foreach ($this->rules as $rule)
            {
                if (
                    (!method_exists($this, 'validateAction' . $rule['action'])) //||
                                                                                //(!is_callable($this, 'validateAction' . $rule['action']))
                )
                {
                    $this->errors['result'] = false;
                    $this->errors['common'] = 'Правило валидации ' . $rule['action'] . ' не существует';
                    break;
                }
            }
        }

        return $this->errors['result'];
    }

    /**
     * Проверят входящие данные
     *
     * @return mixed
     */
    public function checkData()
    {
        if ($this->errors['result'])
        {
            if (
                (!is_array($this->data)) ||
                (!count($this->data))
            )
            {
                $this->errors['result'] = false;
                $this->errors['common'] = 'Некорректные входные данные';
            }

            if ($this->errors['result'])
            {
                foreach ($this->data as $k => $v)
                {
                    if (
                        (!is_string($k)) ||
                        (!is_string($v))
                    )
                    {
                        $this->errors['result'] = false;
                        $this->errors['common'] = 'Некорректные входные данные';
                        break;
                    }
                }
            }
        }

        return $this->errors['result'];
    }

    /**
     * @param  $data
     * @return mixed
     */
    public function validate()
    {

        if ($this->checkRules() && $this->checkData())
        {

            foreach ($this->rules as $rule)
            {
                $this->current_rule   = $rule;
                $this->current_action = $rule['action'];
                call_user_func([$this, 'validateAction' . $rule['action']]);
            }

        }
    }

    /**
     * @return mixed
     */
    public function makeValidateErrorMessage()
    {
        $validateErrorMessage = $this->validateErrorMessages[$this->current_action];
        if (isset($this->current_rule['params']['message']))
        {
            $validateErrorMessage = $this->current_rule['params']['message'];
        }
        $this->currentErrorMessage = $validateErrorMessage;
        $this->currentErrorMessage = str_replace('%field%', $this->currentFieldLabel, $this->currentErrorMessage);

        foreach ($this->current_rule['params'] as $key => $value)
        {
            $this->currentErrorMessage = str_replace('%' . $key . '%', $value, $this->currentErrorMessage);
        }

        return $this->currentErrorMessage;
    }

    /**
     * @param $field
     */
    public function makeFieldLabel($field)
    {
        $this->currentFieldLabel = $field;
        if (isset($this->labels[$field]))
        {
            $this->currentFieldLabel = $this->labels[$field];
        }
    }

    /**
     * Проверяет на заполненность поля
     */
    public function validateActionRequired()
    {
        foreach ($this->current_rule['fields'] as $field)
        {
            if (!isset($this->errors['fields'][$field]))
            {
                $this->makeFieldLabel($field);
                if (!isset($this->data[$field]))
                {
                    $this->makeValidateErrorMessage();
                    $this->errors['result']           = false;
                    $this->errors['fields'][$field][] = $this->currentErrorMessage;
                }
            }
        }
    }

    /**
     * Проверяет минимальную длину строки
     */
    public function validateActionStringMinLength()
    {
        foreach ($this->current_rule['fields'] as $field)
        {
            if (!isset($this->errors['fields'][$field]))
            {
                if (isset($this->current_rule['params']['min']))
                {
                    $this->makeFieldLabel($field);
                    $min = $this->current_rule['params']['min'];
                    if (mb_strlen($this->data[$field]) < $min)
                    {
                        $this->errors['result']           = false;
                        $this->errors['fields'][$field][] = $this->makeValidateErrorMessage();
                    }
                }
            }
        }
    }

    /**
     * Проверяет максимальную длину строки
     */
    public function validateActionStringMaxLength()
    {
        foreach ($this->current_rule['fields'] as $field)
        {
            if (!isset($this->errors['fields'][$field]))
            {
                if (isset($this->current_rule['params']['max']))
                {
                    $this->makeFieldLabel($field);
                    $max = $this->current_rule['params']['max'];
                    if (mb_strlen($this->data[$field]) > $max)
                    {
                        $this->errors['result']           = false;
                        $this->errors['fields'][$field][] = $this->makeValidateErrorMessage();
                    }
                }
            }
        }
    }

    /**
     * Проверяет параметр по регулярному выражению
     */
    public function validateActionRegexTest()
    {
        foreach ($this->current_rule['fields'] as $field)
        {
            if (!isset($this->errors['fields'][$field]))
            {
                if (isset($this->current_rule['params']['regex']))
                {
                    $this->makeFieldLabel($field);
                    $regex = $this->current_rule['params']['regex'];
                    if (!preg_match($regex, $this->data[$field]))
                    {
                        $this->errors['result']           = false;
                        $this->errors['fields'][$field][] = $this->makeValidateErrorMessage();
                    }
                }
            }
        }
    }



    /**
     * Проверяет параметр на соответствие СНИЛСу по маске
     */
    public function validateActionSnils()
    {
        foreach ($this->current_rule['fields'] as $field)
        {
            if (!isset($this->errors['fields'][$field]))
            {
                $this->makeFieldLabel($field);
                $regex = "/^[\d]{3}\-[\d]{3}\-[\d]{3}\s[\d]{2}$/";
                if (!preg_match($regex, $this->data[$field]))
                {
                    $this->errors['result']           = false;
                    $this->errors['fields'][$field][] = $this->makeValidateErrorMessage();
                }
            }
        }
    }
}
