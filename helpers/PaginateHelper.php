<?php

namespace xtetis\xengine\helpers;

/**
 * Хелпер для создания HTML-разметки пагинации.
 */
class PaginateHelper
{
    /**
     * Возвращает HTML-разметку для пагинации.
     *
     * @param  int    $items_per_page       Количество элементов на одной странице.
     * @param  int    $current_page         Текущая страница.
     * @param  int    $total_records        Общее количество записей.
     * @param  string $page_url             Базовый URL для ссылок пагинации.
     * @return string HTML-разметка для пагинации.
     */
    public static function getPagination(
        int    $items_per_page,
        int    $current_page,
        int    $total_records,
        string $page_url
    ): string
    {

        // Минимальная проверка на допустимое значение элементов на странице
        $items_per_page = max(1, $items_per_page);

        // Определение правильного суффикса для параметров URL (определяет, использовать '?' или '&')
        $page_url_suffix = (strpos($page_url, '?') === false) ? '?' : '&';

        // Подсчет общего количества страниц
        $total_pages = (int) ceil($total_records / $items_per_page);

        // Если страниц меньше одной или текущая страница выходит за пределы, пагинация не нужна
        if ($total_pages <= 1 || $current_page > $total_pages)
        {
            return ''; // Пустая строка, т.к. пагинация не требуется
        }

        // Начало HTML-разметки для пагинации
        $pagination = '<ul class="pagination">';

        // Логика для создания ссылок на предыдущие страницы
        if ($current_page > 1)
        {
            $previous_page = $current_page - 1;
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=1" title="First">&laquo;</a></li>';                      // Ссылка на первую страницу
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=' . $previous_page . '" title="Previous">&lt;</a></li>'; // Ссылка на предыдущую страницу
        }

        // Линки к предыдущим двум страницам
        for ($i = max(1, $current_page - 2); $i < $current_page; $i++)
        {
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=' . $i . '">' . $i . '</a></li>';
        }

        // Активная страница (текущая)
        $pagination .= '<li class="page-item active"><a class="page-link">' . $current_page . '</a></li>';

        // Линки к следующим двум страницам
        for ($i = $current_page + 1; $i <= min($total_pages, $current_page + 2); $i++)
        {
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=' . $i . '">' . $i . '</a></li>';
        }

        // Логика для создания ссылок на следующие страницы
        if ($current_page < $total_pages)
        {
            $next_page = $current_page + 1;
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=' . $next_page . '">&gt;</a></li>';                   // Ссылка на следующую страницу
            $pagination .= '<li class="page-item"><a class="page-link" href="' . $page_url . $page_url_suffix . 'p=' . $total_pages . '" title="Last">&raquo;</a></li>'; // Ссылка на последнюю страницу
        }

        // Закрытие тега ul
        $pagination .= '</ul>';

        return $pagination; // Возвращаем HTML-разметку
    }
}
