<?php

namespace xtetis\xengine;


class Component extends \xtetis\xengine\models\Component
{

    /**
     * Обработчик ошибок (стандартный) 
     * Устанавливается в настройках - параметр ERROR_HANDLER_METHOD
     */
    public static function errorHandler(\Throwable $exception)
    {
        $backtrace = '';

        if (\xtetis\xengine\Config::isTest())
        {
            $backtrace = '<br><pre>'.
            print_r($exception,true).
            '</pre>';
        }

        if ($exception->getCode() == 404)
        {
            header("HTTP/1.0 404 Not Found");
        }

        $error_data = "\n\n\n\n\n==========================================================\n";
        $error_data.= date('Y-m-d H:i:s')."\n";
        $error_data.= "==========================================================\n";
        $error_data.= $exception->getMessage()."\n";
        $error_data.= print_r($exception,true)."\n";
        $error_data.= "\n==========================================================\n";

        @file_put_contents(ERRORS_DIRECTORY.'/php_errors.txt',$error_data,FILE_APPEND);
        @chmod(ERRORS_DIRECTORY . '/php_errors.txt',0777);

/*
        \xtetis\xengine\helpers\LogHelper::customDie(
            "Ошибка: " .
            $exception->getMessage().
            $backtrace
        );
        */
        @ob_clean();
        echo \xtetis\xengine\helpers\RenderHelper::renderFile(
            __DIR__.'/views/block/error_layout.php',
            [
                'exception'=>$exception
            ]
        );
        exit;
    }


}
