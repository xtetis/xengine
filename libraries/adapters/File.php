<?php
namespace xtetis\xengine\libraries\adapters;

/**
 * Файловый кеш
 */
class File
{
    /**
     * @var int
     */
    private int $expire;

    /**
     * Теги кеша
     */
    public $tags = [];

    /**
     * Конструктор
     */
    public function __construct(int $expire = 3600)
    {
        $this->expire = $expire;
    }

    /**
     * Оставляет только допустимые символы
     */
    public function clearKey($key = '')
    {
        return preg_replace('/[^A-Z0-9\._-]/i', '', $key);
    }

    /**
     * Получает значение кеша по имени
     */
    public function get(string $key)
    {
        $key   = $this->clearKey($key);
        $files = glob(FILE_CACHE_DIRECTORY . '/cache.' . $key . '.*');

        if ($files)
        {
            $ret = json_decode(file_get_contents($files[0]), true);
        }
        else
        {
            $ret = [];
        }

        return $ret;
    }

    /**
     * Устанавливает значение кеша
     */
    public function set(
        string $key,
               $value,
        int    $expire = 0
    )
    {
        $key = $this->clearKey($key);

        $this->delete($key);

        if (!$expire)
        {
            $expire = $this->expire;
        }

        file_put_contents(FILE_CACHE_DIRECTORY . '/cache.' . $key . '.' . (time() + $expire), json_encode($value));
    }

    /**
     * Удаляет значение кеша (можно удалять по тегу)
     */
    public function delete(string $key)
    {
        $key = $this->clearKey($key);

        $files = glob(FILE_CACHE_DIRECTORY . '/cache.' . $key . '.*');

        if ($files)
        {
            foreach ($files as $file)
            {
                if (!@unlink($file))
                {
                    clearstatcache(false, $file);
                }
            }
        }
    }

    /**
     * Удаляет кеш по тегу
     */
    public function deleteByTag(string $tag_name = '')
    {
        $tag_id  = $this->getCacheTagId($tag_name);
        $tag_str = '_tag' . $tag_id . '_';

        $key = $this->clearKey($tag_str);

        $files = glob(FILE_CACHE_DIRECTORY . '/cache.*' . $key . '.*');

        if ($files)
        {
            foreach ($files as $file)
            {
                if (!@unlink($file))
                {
                    clearstatcache(false, $file);
                }
            }
        }
    }

    /**
     * Удаляет весь кеш
     */
    public function flush(): void
    {
        $files = glob(FILE_CACHE_DIRECTORY . '/cache.*');

        foreach ($files as $file)
        {
            if (!@unlink($file))
            {
                clearstatcache(false, $file);
            }
        }
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        $files = glob(FILE_CACHE_DIRECTORY . '/cache.*');

        if ($files && mt_rand(1, 100) == 1)
        {
            foreach ($files as $file)
            {
                $time = substr(strrchr($file, '.'), 1);

                if ($time < time())
                {
                    if (!@unlink($file))
                    {
                        clearstatcache(false, $file);
                    }
                }
            }
        }
    }

    public function getTotalCount(): int
    {
        $files = glob(FILE_CACHE_DIRECTORY . '/cache.*');

        return count($files);
    }

    /**
     * Возвращает количество файлов кеша для указанного тега
     */
    public function getTagCacheCount($tag_name = ''): int
    {

        $tag_id  = $this->getCacheTagId($tag_name);
        $tag_str = '_tag' . $tag_id . '_';

        $files = glob(FILE_CACHE_DIRECTORY . '/cache.*' . $tag_str . '*');

        return count($files);
    }

    /**
     * Возвращает список тегов
     */
    public function getTagList()
    {
        if (count($this->tags))
        {
            return $this->tags;
        }

        $file_tags = FILE_CACHE_DIRECTORY . '/tags.json';
        $this->tags      = [];
        if (file_exists($file_tags))
        {
            $this->tags = json_decode(@file_get_contents($file_tags), true);
        }

        return $this->tags;
    }

    /**
     * Возвращает ID тега кеша (при этом, если такого нет - добавляет его)
     */
    public function getCacheTagId($tag_name): int
    {
        $tag_name  = strval($tag_name);
        $file_tags = FILE_CACHE_DIRECTORY . '/tags.json';

        $tags = $this->getTagList();

        // Если тег найден
        if (array_search($tag_name, $tags) !== false)
        {
            return intval(array_search($tag_name, $tags));
        }

        $tags[] = $tag_name;

        $tags = array_flip($tags);
        $tags = array_flip($tags);

        file_put_contents($file_tags, json_encode($tags));

        return intval(array_search($tag_name, $tags));
    }

}
