<?php

namespace xtetis\xengine\libraries;


/**
 * Чтобы не создавать заново модель
 */
global $cache;

/**
 * Класс Cache
 */
class Cache
{
    /**
     * @var object
     */
    private object $adaptor;

    /**
     * Constructor
     */
    public function __construct(
        string $adaptor = 'File',
        int    $expire = CACHE_LIFETIME
    )
    {
        $class = 'xtetis\\xengine\\libraries\\adapters\\' . $adaptor;

        if (class_exists($class))
        {
            $this->adaptor = new $class($expire);
        }
        else
        {
            throw new \Exception('не удается загрузить адаптер ' . $class . ' cache!');
        }

        global $cache;
        $cache = $this;
    }



    /**
     * Возвращает объект Cache (стобы не создавать его заново)
     */
    public static function getCacheObj()
    {
        /**
         * Чтобы не создавать заново модель
         */
        global $cache;

        if (!isset($cache))
        {
            $cache = new self();
        }

        return $cache;

    }

    /**
     * Получает кеш по имени
     */
    public function get(string $key)
    {
        return $this->adaptor->get($key);
    }

    /**
     * Сохраняет значение кеша
     */
    public function set(
        string $key,
               $value,
        int    $expire = 0
    )
    {
        $this->adaptor->set($key, $value, $expire);
    }

    /**
     * Удаляет кеш
     */
    public function delete(string $key)
    {
        $this->adaptor->delete($key);
    }

    /**
     * Удаляет весь кеш
     */
    public function flush()
    {
        $this->adaptor->flush();
    }

    /**
     * Возвращает количество сохраненноно кеша
     */
    public function getTotalCount()
    {
        return $this->adaptor->getTotalCount();
    }

    /**
     * Возвращает ID тега кеша
     */
    public function getCacheTagId($tag_name = '')
    {
        return $this->adaptor->getCacheTagId($tag_name);
    }

    /**
     * Удаляет кеш по тегу
     */
    public function deleteByTag($tag_name = '')
    {
        return $this->adaptor->deleteByTag($tag_name);
    }



    /**
     * Возвращает список тегов
     */
    public function getTagList()
    {
        return $this->adaptor->getTagList();
    }



    /**
     * Возвращает список тегов
     */
    public function getTagCacheCount($tag_name = '')
    {
        return $this->adaptor->getTagCacheCount($tag_name);
    }



    /**
     * Генерирует ID кеша по параметрам
     */
    public function genSqlCacheId(
        $sql = '',
        $sql_params = [],
        $tags = []
    )
    {
        $sql      = strval($sql);
        $cache_id = 'sql_' . md5($sql . json_encode($sql_params));
        if (!is_array($tags))
        {
            $tags = [];
        }

        $cache_tags = [];
        foreach ($tags as $tag)
        {
            if (strlen(strval($tag)))
            {
                $cache_tags[] = strval($tag);
            }
        }

        $tag_id_list = [];
        foreach ($cache_tags as $tag_name)
        {
            $tag_id_list[] = $this->getCacheTagId($tag_name);
        }

        foreach ($tag_id_list as $tag_id)
        {
            $cache_id .= '_tag' . $tag_id . '_';
        }

        return $cache_id;

    }
}
