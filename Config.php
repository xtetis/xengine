<?php

namespace xtetis\xengine;

/**
 * Сласс для валидации параметром компонента
 */
class Config extends \xtetis\xengine\models\Model
{
    /**
     * Загрузка конфигов
     */
    public static function setConfig($config_directory = '')
    {
        if (!file_exists($config_directory))
        {
            die('Не найдена директория с настройками config_directory в ' . __FILE__);
        }

        if (!is_dir($config_directory))
        {
            die('Путь указанный как директория с настройками '.$config_directory.' не является директорией');
        }

        if (!is_readable($config_directory))
        {
            die('Директория '.$config_directory.' недоступна для чтения');
        }


        /**
         * Подключаем все конфиги из директории конфигов
         */
        foreach (glob($config_directory . '/*.php') as $file)
        {
            if (is_file($file))
            {
                require $file;
            }
        }




        define('CONFIG_DIRECTORY', $config_directory);

        self::validateParams();
        
        // Устанавливаем роуты
        self::setRoutes();

    }

    /**
     * Настройка роутов
     */
    public static function setRoutes()
    {

        $routes = self::getConfigArray('params.routes');

        $component_url_list = \xtetis\xengine\Config::getComponentUrlList();
        
        foreach ($component_url_list as $component_url => $component_class) 
        {
            $component_routes = $component_class::getComponentRoutes();

            $routes = array_merge($component_routes,$routes);
        }

        foreach ($routes as $route)
        {
            \xtetis\xengine\helpers\RouterHelper::route($route['url'], $route['function']);
        }



        $sef = false;
        if (defined('SEF'))
        {
            $sef = SEF;
        }

        $rurl = $_SERVER['REQUEST_URI'];

        if ($sef)
        {
            $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        }
        else
        {
            $path = \xtetis\xengine\helpers\RequestHelper::get('_path','str','');
            $path = '/'.$path;
        }

        \xtetis\xengine\helpers\RouterHelper::execute($path);

    }

    /**
     * Возвращает массив из файла конфига
     *
     * @param  $name
     * @return mixed
     */
    public static function getConfigArray($config_name)
    {
        $ret      = [];
        $filename = CONFIG_DIRECTORY . '/' . $config_name . '.php';
        if (file_exists($filename))
        {
            $ret = require $filename;
        }

        return $ret;
    }

    /**
     * Валидирует параметры движка
     */
    public static function validateParams()
    {
        if (!defined('COMPONENT_URL_LIST'))
        {
            define('COMPONENT_URL_LIST', serialize([]));
        }

        $component_url_list = COMPONENT_URL_LIST;
        if (!is_string($component_url_list))
        {
            die('Параметр COMPONENT_URL_LIST (Урлы компонентов) должен быть строкой, сериализованным массивом');
        }

        $component_url_list = unserialize($component_url_list);
        if (!is_array($component_url_list))
        {
            die('Параметр COMPONENT_URL_LIST (Урлы компонентов) должен быть сериализованным массивом');
        }

        foreach ($component_url_list as $url => $component)
        {
            if (!is_string($url))
            {
                die('Ключами в сериализованном массиве COMPONENT_URL_LIST (Урлы компонентов) должены быть строки');
            }

            if (!is_string($component))
            {
                die('Значениями в сериализованном массиве COMPONENT_URL_LIST (Урлы компонентов) должены быть строки - имена компонентов');
            }
        }

        if (!defined('DEFAULT_LAYOUT'))
        {
            define('DEFAULT_LAYOUT', 'index');
        }

        if (!defined('ENGINE_DIRECTORY'))
        {
            define('ENGINE_DIRECTORY', __DIR__.'/../../..');
        }

        if (!is_string(DEFAULT_LAYOUT))
        {
            die('Параметр DEFAULT_LAYOUT должен быть строкой');
        }

        if (!file_exists(ENGINE_DIRECTORY.'/views/layout/'.DEFAULT_LAYOUT.'.php'))
        {
            die('Шаблон, указанный в  DEFAULT_LAYOUT ('.DEFAULT_LAYOUT.') не существует');
        }

        if (!defined('ASSETS_DIRECTORY'))
        {
            // Директория, где хранятся статические ресурсы
            define('ASSETS_DIRECTORY', realpath(ENGINE_DIRECTORY . '/../assets'));
        }

        if (!is_string(ASSETS_DIRECTORY))
        {
            die('Параметр ASSETS_DIRECTORY должен быть строкой');
        }

        if (!file_exists(ASSETS_DIRECTORY))
        {
            die('Параметр ASSETS_DIRECTORY должен быть существующей директорией');
        }

        if (!is_dir(ASSETS_DIRECTORY))
        {
            die('Параметр ASSETS_DIRECTORY должен быть существующей директорией');
        }

        if (!defined('GENERATED_STAT_RES_DIRECTORY'))
        {
            // Директория, где хранятся сгенерированные статические ресурсы
            // Используются для модулей
            define('GENERATED_STAT_RES_DIRECTORY', ASSETS_DIRECTORY.'/generated');
        }

        

        if (!is_string(GENERATED_STAT_RES_DIRECTORY))
        {
            die('Параметр GENERATED_STAT_RES_DIRECTORY должен быть строкой');
        }

        if (!file_exists(GENERATED_STAT_RES_DIRECTORY))
        {
            die('Параметр GENERATED_STAT_RES_DIRECTORY должен быть существующей директорией ('.GENERATED_STAT_RES_DIRECTORY.')');
        }

        if (!is_dir(GENERATED_STAT_RES_DIRECTORY))
        {
            die('Параметр GENERATED_STAT_RES_DIRECTORY должен быть существующей директорией ('.GENERATED_STAT_RES_DIRECTORY.')');
        }

        
        if (!is_writable(GENERATED_STAT_RES_DIRECTORY))
        {
            die('Директория, указанная в GENERATED_STAT_RES_DIRECTORY ('.GENERATED_STAT_RES_DIRECTORY.') должна быть доступна для записи');
        }

        if (!defined('SEF'))
        {
            define('SEF', false);
        }

        if (!defined('RUNTIME_DIRECTORY'))
        {
            // Директория RUNTIME
            define('RUNTIME_DIRECTORY', ENGINE_DIRECTORY . '/runtime');
        }

        // По-умолчанию дополнительный обработчик SEF - не включен
        if (!defined('SEF_HANDLER_METHOD'))
        {
            define('SEF_HANDLER_METHOD','');
        }

        // Параметрфы кеширования
        // ------------------------------------------
        // По-умолчанию дополнительный обработчик SEF - не включен
        if (!defined('CACHE_LIFETIME'))
        {
            define('CACHE_LIFETIME',60*60*24);
        }
        
        $cache_lifetime = intval(CACHE_LIFETIME);
        if ($cache_lifetime !== CACHE_LIFETIME)
        {
            die('Параметр CACHE_LIFETIME должен быть целочисленным');
        }

        if (CACHE_LIFETIME <=0)
        {
            die('Параметр CACHE_LIFETIME должен быть больше чем 0');
        }
        // ------------------------------------------

        if (!defined('ERRORS_DIRECTORY'))
        {
            // Директория ERRORS
            define('ERRORS_DIRECTORY', RUNTIME_DIRECTORY . '/errors');
        }

        if (!file_exists(ERRORS_DIRECTORY))
        {
            die('Параметр ERRORS_DIRECTORY должен быть существующей директорией ('.ERRORS_DIRECTORY.')');
        }

        if (!is_dir(ERRORS_DIRECTORY))
        {
            die('Параметр ERRORS_DIRECTORY должен быть существующей директорией ('.ERRORS_DIRECTORY.')');
        }

        if (!is_writable(ERRORS_DIRECTORY))
        {
            die('Директория, указанная в ERRORS_DIRECTORY должна быть доступна для записи ('.ERRORS_DIRECTORY.')');
        }

        // ------------------------------------------

        if (!defined('FILE_CACHE_DIRECTORY'))
        {
            // Директория ERRORS
            define('FILE_CACHE_DIRECTORY', RUNTIME_DIRECTORY . '/cache');
        }

        if (!file_exists(FILE_CACHE_DIRECTORY))
        {
            die('Параметр FILE_CACHE_DIRECTORY  должен быть существующей директорией ('.FILE_CACHE_DIRECTORY.')');
        }

        if (!is_dir(FILE_CACHE_DIRECTORY))
        {
            die('Параметр FILE_CACHE_DIRECTORY должен быть существующей директорией ('.FILE_CACHE_DIRECTORY.')');
        }

        if (!is_writable(FILE_CACHE_DIRECTORY))
        {
            die('Директория, указанная в FILE_CACHE_DIRECTORY должна быть доступна для записи ('.FILE_CACHE_DIRECTORY.')');
        }

        // ------------------------------------------

        if (!function_exists('mb_substr'))
        {
            die('На сервере не установлено расширение mbstring');
        }


        // Шаблон для отображения ошибки
        if (!defined('ERROR_LAYOUT_FILENAME'))
        {
            define(
                'ERROR_LAYOUT_FILENAME',
                __DIR__ . '/views/block/error_layout.php'
            );
        }

        if (!is_string(ERROR_LAYOUT_FILENAME))
        {
            die('Значение ERROR_LAYOUT_FILENAME не является строкой');
        }

        if (!file_exists(ERROR_LAYOUT_FILENAME))
        {
            die('Файл, указанный в значении ERROR_LAYOUT_FILENAME не существует');
        }


        // Шаблон для отображения страницы с ошибкой внутри текущего шаблона
        if (!defined('ERROR_USER_PAGE'))
        {
            define(
                'ERROR_USER_PAGE',
                __DIR__ . '/views/block/page_user_error.php'
            );
        }

        if (!is_string(ERROR_USER_PAGE))
        {
            die('Значение ERROR_USER_PAGE не является строкой');
        }

        if (!file_exists(ERROR_USER_PAGE))
        {
            die('Файл, указанный в значении ERROR_USER_PAGE не существует');

        }

        //  =============================================

        // Шаблон для отображения страницы с ошибкой внутри текущего шаблона
        if (!defined('ERROR_LEVEL_ARRAY'))
        {
            define('ERROR_LEVEL_ARRAY',[
                E_ERROR             => TRUE,
                E_WARNING           => TRUE,
                E_PARSE             => TRUE,
                E_NOTICE            => TRUE,
                E_CORE_ERROR        => TRUE,
                E_CORE_WARNING      => TRUE,
                E_COMPILE_ERROR     => TRUE,
                E_COMPILE_WARNING   => TRUE,
                E_USER_ERROR        => TRUE,
                E_USER_WARNING      => TRUE,
                E_USER_NOTICE       => TRUE,
                E_STRICT            => TRUE,
                E_RECOVERABLE_ERROR => TRUE,
                E_DEPRECATED        => FALSE,
                E_USER_DEPRECATED   => TRUE,
                E_ALL               => TRUE,
            ]);
        }


        if (!is_array(ERROR_LEVEL_ARRAY))
        {
            die('Значение ERROR_LEVEL_ARRAY не является массивом');
        }

        //  =============================================
        
        // Коды ошибок, для обработчика ошибок
        if (!defined('ERROR_HANDLER_DEFAULT_HTTP_CODES'))
        {
            define('ERROR_HANDLER_DEFAULT_HTTP_CODES',[
                'ErrorException'            => 500,
                'ParseException'            => 500,
                'NoticeException'           => 200,
                'StrictException'           => 500,
                'WarningException'          => 200,
                'CoreErrorException'        => 500,
                'UserErrorException'        => 200,
                'DeprecatedException'       => 500,
                'UserNoticeException'       => 200,
                'CoreWarningException'      => 500,
                'UserWarningException'      => 200,
                'CompileErrorException'     => 500,
                'CompileWarningException'   => 500,
                'UserDeprecatedException'   => 200,
                'RecoverableErrorException' => 500,
            ]);
        }

        if (!is_array(ERROR_HANDLER_DEFAULT_HTTP_CODES))
        {
            die('Значение ERROR_HANDLER_DEFAULT_HTTP_CODES не является массивом');
        }


        return true;
        
    }

    /**
     * Возвращает список урлов для компонентов xengine в виде
     * url > class
     */
    public static function getComponentUrlList()
    {

        // Валидирует параметры движка
        self::validateParams();

        $component_url_list = unserialize(COMPONENT_URL_LIST);
        
        foreach ($component_url_list as $component_url => $component_class) 
        {
            // Проверяем, что указанные классы, являются наследниками Component
            if (!\xtetis\xengine\models\Component::isChildOfComponent($component_class))
            {
                die($component_class.' не является наследником компонента '.\xtetis\xengine\models\Component::class);
            }
        }

        return $component_url_list;
    }

    /**
     * Если установлено в конфиге ERROR_HANDLER_METHOD - 
     * устанавливаем обработчик ошибок
     */
    public static function setErrorHandler()
    {
        if (!defined('EXCEPTION_HANDLER_METHOD'))
        {
            // Метод, который обрабатывает ошибки PHP
            define('EXCEPTION_HANDLER_METHOD', '\xtetis\xengine\helpers\ErrorHelper::exceptionHandler');
        }

        if (!defined('ERROR_HANDLER_METHOD'))
        {
            // Метод, который обрабатывает ошибки PHP
            define('ERROR_HANDLER_METHOD', '\xtetis\xengine\helpers\ErrorHelper::errorHandler');
        }


        
        $exception_handler_method = EXCEPTION_HANDLER_METHOD;
        if (is_string($exception_handler_method))
        {
            if (strlen($exception_handler_method))
            {
                if (is_callable($exception_handler_method))
                {
                    set_exception_handler($exception_handler_method);
                }
                else
                {
                    throw new \Exception('EXCEPTION_HANDLER_METHOD is not callable');
                }
            }
        }





        $error_handler_method = ERROR_HANDLER_METHOD;
        if (is_string($error_handler_method))
        {
            if (strlen($error_handler_method))
            {
                if (is_callable($error_handler_method))
                {
                    set_error_handler($error_handler_method);
                }
                else
                {
                    throw new \Exception('ERROR_HANDLER_METHOD is not callable');
                }
            }
        }
        
    }

    /**
     * Если установлено в конфиге SEF_HANDLER_METHOD - 
     * устанавливаем дополнительный метод для обработки SEF страниц
     */
    public static function getSefHandler()
    {

        if (!defined('SEF_HANDLER_METHOD'))
        {
            return false;
        }
        
        

        $sef_handler_method = SEF_HANDLER_METHOD;
        if (is_string($sef_handler_method))
        {
            if (strlen($sef_handler_method))
            {
                if (is_callable($sef_handler_method))
                {
                    return $sef_handler_method;
                }
                else
                {
                    throw new \Exception('SEF_HANDLER_METHOD is not callable');
                }
            }
        }
        
        return false;

    }

    

    /**
     * Установлено ли в настройках тестовый режим (отдалка включена)
     */
    public static function isTest()
    {
        if (!defined('MODE_TEST'))
        {
            return false;
        }

        return MODE_TEST;
    }
}
