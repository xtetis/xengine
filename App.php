<?php

namespace xtetis\xengine;

/**
 * Чтобы не создавать заново модель,  а все время работать с одной
 */
global $app;

/**
 * Класс приложения APP xEngine
 */
class App extends \xtetis\xengine\models\Model
{

    /**
     * @var float Время старта приложения
     */
    protected $start_microtime = 0;

    /**
     * Текущий компонента
     */
    public $component_name = '';

    /**
     * Текущее действие
     */
    public $action_name = '';

    /**
     * Текущий запрос
     */
    public $query_name = '';

    /**
     * Префикс класса компонента
     */
    public $component_class_prefix = '';

    /**
     * Префикс и имя класса компонента
     */
    public $component_class_name = '';
    /**
     * @var string
     */
    public $component_parent_class_name = '';

    /**
     * Объект компонента 
     */
    public $component_object = false;

    /**
     * Компонент APP
     */
    public $component_app_object = false;

    /**
     * @var string Файл действия компонента
     */
    public $action_filename = '';

    /**
     * @var array Дополнительные параметры приложения
     */
    public $additional_params = [];


    /**
     * Конструктор приложения
     * @param array $params Параметры для инициализации
     */
    public function __construct($params = [])
    {
        global $app;
        $app = $this;

        // Проверка параметров конфигурации
        \xtetis\xengine\Config::validateParams();

        // Устанавливаем шаблон по умолчанию
        self::setParam('layout', DEFAULT_LAYOUT);
    }

    /**
     * Получение экземпляра приложения (Singleton)
     * 
     * @return App
     */
    public static function getApp()
    {
        global $app;

        if (!isset($app)) {
            $app = new self();
        }

        return $app;
    }

    /**
     * Установка параметра приложения
     * 
     * @param string $param_name Имя параметра
     * @param mixed $param_value Значение параметра
     * @param bool $overwrite Перезаписывать ли существующее значение
     */
    public static function setParam($param_name, $param_value, $overwrite = true)
    {
        if ($overwrite || !isset(self::getApp()->additional_params[$param_name])) {
            self::getApp()->additional_params[$param_name] = $param_value;
        }
    }


    /**
     * Получение значения параметра приложения
     * 
     * @param string $param_name Имя параметра
     * @param mixed $default Значение по умолчанию
     * @return mixed
     */
    public static function getParam($param_name = 'title', $default = '')
    {
        return self::getApp()->additional_params[$param_name] ?? $default;
    }

    /**
     * Получение текущего шаблона (layout)
     * 
     * @return string
     */
    public static function getLayout()
    {
        return self::getParam('layout') ?: self::getApp()->getAction();
    }

    /**
     * Запуск приложения
     */
    public function run()
    {
        $this->start_microtime = microtime(true);
        
        // Устанавливаем обработчик ошибок
        \xtetis\xengine\Config::setErrorHandler();
        

        $this->runComponent();
    }

    /**
     * Запуск дополнительного SEF обработчика
     * 
     * @param string $url URL для обработки
     * @return mixed
     */
    public function runAdditionalSefHandler($url = '')
    {
        $sefHandler = \xtetis\xengine\Config::getSefHandler();
        return $sefHandler ? call_user_func($sefHandler, $url) : false;
    }

    /**
     * Рендер страницы для текущего компонента
     * 
     * @param array $data Данные для рендеринга
     * @return string
     */
    public function renderCurrentPage($data = [])
    {
        return \xtetis\xengine\helpers\RenderHelper::renderPage(
            $this->getActionQuery(),
            $data
        );
    }

    /**
     * Получение текущего действия/запроса компонента
     * 
     * @return string|false
     */
    public function getActionQuery()
    {
        return $this->getComponent() ? $this->getComponent()->getActionQuery() : false;
    }

    /**
     * Проверка активности действия/запроса компонента
     * 
     * @param string $action_query Действие/запрос
     * @return bool
     */
    public function isActiveActionQuery($action_query)
    {
        return $this->getActionQuery() === $action_query;
    }

    /**
     * Получение текущего действия компонента
     * 
     * @return string|false
     */
    public function getAction()
    {
        return $this->getComponent() ? $this->getComponent()->getAction() : false;
    }

    /**
     * Получение текущего запроса компонента
     * 
     * @return string|false
     */
    public function getQuery()
    {
        return $this->getComponent() ? $this->getComponent()->getQuery() : false;
    }

    /**
     * Получение объекта текущего компонента
     * 
     * @return object|false
     */
    public function getComponent()
    {
        return $this->component_object;
    }

    /**
     * Получение класса компонента приложения
     * 
     * @return string
     */
    public function getAppComponentClass()
    {
        return \xtetis\xengine\Component::class;
    }

    /**
     * Запускает действие в модуле компонента
     * [
     *           'component_name'=>'manager',
     *           'component_path'=>'/default/index',
     *
     * ]
     */
    public function runComponent($params = [])
    {

        $this->component_name = strval($this->component_name);
        $this->action_name    = strval($this->action_name);
        $this->query_name     = strval($this->query_name);

        $this->component_name = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->component_name);
        $this->action_name    = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->action_name);
        $this->query_name     = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->query_name);

        // Константы, которые должны проверяться в контроллере
        $required_constants = [];
        if (isset($params['required_constants']))
        {
            if (is_array($params['required_constants']))
            {
                foreach ($params['required_constants'] as $constant_name)
                {
                    if (is_string($constant_name))
                    {
                        $required_constants[] = $constant_name;
                    }
                }
            }
        }

        if (!strlen($this->component_name))
        {
            $this->component_name = 'app';
        }

        if (!strlen($this->action_name))
        {
            $this->action_name = 'default';
        }

        if (!strlen($this->query_name))
        {
            $this->query_name = 'index';
        }

        if ('app' == $this->component_name)
        {
            $this->component_class_prefix = 'xtetis\xengine\Component';
        }
        else
        {
            $component_url_list = \xtetis\xengine\Config::getComponentUrlList();

            $this->component_class_prefix = $component_url_list[$this->component_name];
        }

        $this->component_class_name = $this->component_class_prefix;

        if (!class_exists($this->component_class_name))
        {
            \xtetis\xengine\helpers\LogHelper::customDie(__FUNCTION__ . ': Класс ' . $this->component_class_name . ' не существует');
        }

        $this->component_parent_class_name = get_parent_class($this->component_class_name);

        if (!$this->component_parent_class_name)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Для компонента ' . $this->component_class_name . ' не установлен родительский класс');
        }

        if ('xtetis\xengine\models\Component' != $this->component_parent_class_name)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Для модуля ' . $this->component_class_name . ' родительский класс не \xtetis\xengine\models\Component');
        }

        $component_class_name   = $this->component_class_name;
        $this->component_object = new $component_class_name([
            'component'          => $this->component_name,
            'action'             => $this->action_name,
            'query'              => $this->query_name,
            // Список констант, которые должны проверяться в контроллере
            'required_constants' => $required_constants,
        ]);

        $this->component_object->runComponent();

        if ($this->component_object->getErrors())
        {
            \xtetis\xengine\helpers\LogHelper::customDie($this->component_class_name . ': ' . $this->component_object->getLastErrorMessage());
        }

        return true;
    }

    /**
     * Возвращает время с начала выполнения приложения
     */
    public function getCurrentRunTime()
    {
        $current_time = microtime(true);
        $time         = $current_time - $this->start_microtime;

        return $time;
    }

    /**
     * Возвращает - является ли переданное имя класса компонента
     * активным для приложения
     */
    public function isActiveComponentClass($class_name = '')
    {
        $class_name = strval($class_name);
        if ($this->getComponent())
        {
            return ($class_name == $this->getComponent()::class);
        }
        else
        {
            throw new \Exception('Компонент не определен');

        }
    }

}
