<?php

namespace xtetis\xengine\models;

/**
 * Модель для наследования
 */
class Model
{
    /**
     * Список ошибок при работе функций
     */
    protected $errors = [];

    /**
     * Возвращет ошибки
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        foreach ($params as $k => $v)
        {
            if (property_exists($this, $k))
            {
                $this->$k = $v;
            }
        }

    }

    /**
     * Возвращет текст последней ошибки
     */
    public function getLastErrorMessage($add_category = false)
    {
        $ret = '';
        if ($this->getErrors())
        {
            $errors = $this->getErrors();
            if ((is_array($errors)) && ($errors))
            {
                $array_values   = array_values($errors);
                $error_category = array_shift($array_values);
                if ((is_array($error_category)) && ($error_category))
                {
                    $array_values = array_values($error_category);

                    if ($add_category)
                    {
                        $ret = $error_category . ': ' . array_shift($array_values);
                    }
                    else
                    {
                        $ret = array_shift($array_values);
                    }

                }
            }

        }

        return $ret;
    }

    /**
     * Очищает массив ошибок
     */
    public function clearErrors()
    {
        $this->errors = [];
    }

    /**
     * Возвращет текст последней ошибки ддля аттрибуута, иначе false
     */
    public function getAttributeLastErrorMessage($attribute_name)
    {
        $ret = false;
        if ($this->getErrors())
        {
            $errors = $this->getErrors();
            if ((isset($errors[$attribute_name])) && ($errors[$attribute_name]))
            {
                $array_values = array_values($errors[$attribute_name]);
                $ret          = array_shift($array_values);
            }
        }

        return $ret;
    }

    /**
     * Возвращет ошибки
     */
    public function addError(
        $attribute_name,
        $error_message
    )
    {
        $this->errors[$attribute_name][] = $error_message;
    }

    /**
     * Получает текущую модель. При этом, если в сессии хранится
     * текущая модель - возвращает её, иначе создает экземпляр класса
     *
     * @param  $identifier
     * @param  array           $params
     * @param  $create_model
     * @return mixed
     */
    public static function getModel(
        $identifier = '',
        $params = [],
        $create_model = true
    )
    {
        $model = false;

        if (strlen($identifier))
        {
            if (isset($_SESSION[get_called_class()][$identifier]))
            {
                $model = unserialize($_SESSION[get_called_class()][$identifier]);
            }
        }
        else
        {
            if ((isset($_SESSION[get_called_class()])) && (is_string($_SESSION[get_called_class()])))
            {
                $model = unserialize($_SESSION[get_called_class()]);
            }
        }

        if (
            (is_object($model)) &&
            (get_class($model) == get_called_class()) &&
            (!$model->getErrors())
        )
        {
            return $model;
        }

        if ($create_model)
        {
            $class = get_called_class();
            $model = new $class($params);
        }

        return $model;
    }

    /**
     * Возвращает массив идентификаторов по которым был сохранен массив моделей
     */
    public static function getSavedModelIdentifiers()
    {
        if ((isset($_SESSION[get_called_class()])) && (is_array($_SESSION[get_called_class()])))
        {
            return array_keys($_SESSION[get_called_class()]);
        }

        return [];
    }

    /**
     * Сохраняет текущую модель (объект) в сессионной переменной
     */
    public function saveModel($identifier = '')
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (strlen($identifier))
        {
            $_SESSION[get_called_class()][$identifier] = serialize($this);
        }
        else
        {
            if (
                (isset($_SESSION[get_called_class()])) &&
                (is_array($_SESSION[get_called_class()]))
            )
            {
                die('Некорректная попытка сохранения модели');
            }
            $_SESSION[get_called_class()] = serialize($this);
        }
    }

    /**
     * Очищает кеш запросов
     */
    public static function flushModel($identifier = '')
    {
        if (strlen($identifier))
        {
            if (isset($_SESSION[get_called_class()][$identifier]))
            {
                unset($_SESSION[get_called_class()][$identifier]);
            }
        }
        else
        {
            if (isset($_SESSION[get_called_class()]))
            {
                unset($_SESSION[get_called_class()]);
            }
        }
    }

    /**
     * Загружает данные из POST параметров
     *
     * $params = [
     *      'prop_name' => [
     *          'type'=>'raw' (int|raw|str|arr)
     *          'default'=>'' ('',0,[])
     *      ]
     * ];
     *
     */
    public function loadPostData($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        foreach (get_object_vars($this) as $key => $value)
        {
            if ($params)
            {
                if (isset($params[$key]))
                {
                    $default = '';
                    if (isset($params[$key]['default']))
                    {
                        $default = $params[$key]['default'];
                    }

                    $type = 'raw';
                    if (isset($params[$key]['type']))
                    {
                        $type = $params[$key]['type'];
                    }

                    if (!in_array($type, ['int', 'raw', 'str', 'arr']))
                    {
                        $type = 'raw';
                    }

                    $this->$key = \xtetis\xengine\helpers\RequestHelper::post($key, $type, $default);
                }
            }
            else
            {
                if (isset($_POST[$key]))
                {
                    if (is_long($this->$key))
                    {
                        $this->$key = \xtetis\xengine\helpers\RequestHelper::post($key, 'float', '');
                    }
                    elseif (is_integer($this->$key))
                    {
                        $this->$key = \xtetis\xengine\helpers\RequestHelper::post($key, 'int', 0);
                    }
                    elseif (is_string($this->$key))
                    {
                        $this->$key = \xtetis\xengine\helpers\RequestHelper::post($key, 'str', '');
                    }
                    else
                    {
                        $this->$key = \xtetis\xengine\helpers\RequestHelper::post($key, 'raw', '');
                    }

                }
            }

        }
    }

}
