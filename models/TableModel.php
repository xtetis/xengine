<?php

namespace xtetis\xengine\models;

/**
 * Модель TableModel для наследования в компонентах - представляет собой
 */
class TableModel extends \xtetis\xengine\models\Model
{

    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = '';

    /**
     * Проверено существование таблицы
     */
    public $table_exists_checked = false;

    /**
     * Список полей
     */
    public $fields = [

    ];

    /**
     * @var int
     */
    public $id = 0;

    /**
     * @var mixed
     */
    public $connect = DEFAULT_DB;

    /**
     * @var mixed
     */
    public $record_found = false;

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Список полей, которые обновлять
     */
    public $insert_update_field_list = [];

    /**
     * Список правил для связанных классов
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [];

    /**
     * Список связанных моделей по аттрибуту
     */
    public $model_related_list = [];

    /**
     * @var array
     */
    protected $fields_to_update = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        foreach ($params as $k => $v)
        {
            if (property_exists($this, $k))
            {
                $this->$k = $v;
            }
        }
    }

    /**
     * Валидирует имя таблицы
     */
    public function validateTable()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // если ранее проверено существование таблицы
        if ($this->table_exists_checked)
        {
            return true;
        }

        $this->table_name = strval($this->table_name);

        if (!strlen($this->table_name))
        {
            throw new \Exception('Не указана таблица для модели TableModel');

            return false;
        }

        $this->result_sql['getTableExists'] = $this->getTableExists();

        if (!$this->result_sql['getTableExists'])
        {
            throw new \Exception('Таблица для модели TableModel не существует');

            return false;
        }

        return true;
    }

    /**
     * Валидирует ID
     */
    public function validateId()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID');

            return false;
        }

        return true;
    }

    /**
     * Проставляет аттрибуты модели согласно записи в таблице по ID
     */
    public function getModelById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateId())
        {
            return false;
        }

        if (!$this->validateTable())
        {
            return false;
        }

        $this->result_sql['getTableRecordById'] = $this->getTableRecordById();

        if ($this->result_sql['getTableRecordById'])
        {
            foreach ($this->result_sql['getTableRecordById'] as $field_name => $field_value)
            {
                if (property_exists($this, $field_name))
                {
                    $this->$field_name = $field_value;
                }
            }
        }
        else
        {
            $this->addError('id', 'Запись #' . $this->id . ' не найдена в таблице ' . $this->table_name);

            return false;
        }

        return true;
    }

    /**
     * Возвращает модель по ID или false, в случае неудачи
     */
    public static function generateModelById($id = 0)
    {
        $id           = intval($id);
        $called_class = get_called_class();
        $model        = new $called_class;
        $model->id    = $id;
        $model->getModelById();
        if ($model->getErrors())
        {
            return false;
        }
        else
        {
            return $model;
        }
    }

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     */
    public function getModelRelated(
        $attribute_name = ''
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen($attribute_name))
        {
            $this->addError('attribute_name', 'Не указан аттрибут для получения связанного класса');

            return false;
        }

        if (!isset($this->related_rule_list[$attribute_name]))
        {
            $this->addError('attribute_name', 'В правилах related_rule_list не указана модель для аттрибута ' . $attribute_name);

            return false;
        }

        $related_class = $this->related_rule_list[$attribute_name];

        if (!strlen($related_class))
        {
            $this->addError('related_class', 'Не указан класс модели связанного класса');

            return false;
        }

        if (!class_exists($related_class))
        {
            $this->addError('related_class', 'Класс ' . $related_class . ' не существует');

            return false;
        }

        $parent_class_name = get_parent_class($related_class);
        if (!$parent_class_name)
        {
            $this->addError('parent_class_name', 'Для модели ' . $related_class . ' не установлен родительский класс');

            return false;
        }

        if (self::class != $parent_class_name)
        {
            $this->addError('parent_class_name', 'Для модели ' . $related_class . ' родительский класс не ' . self::class);

            return false;
        }

        if (isset($this->model_related_list[$attribute_name]))
        {
            return $this->model_related_list[$attribute_name];
        }

        $this->model_related_list[$attribute_name] = $related_class::generateModelById(
            $this->$attribute_name
        );

        return $this->model_related_list[$attribute_name];
    }

    /**
     * Возвращает запись таблицы по ID
     */
    public function getTableRecordById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateId())
        {
            return false;
        }

        if (!$this->validateTable())
        {
            return false;
        }

        // Время начала работы запроса
        $start_microtime = microtime(true);

        $sql = 'SELECT * FROM ' . $this->table_name . ' WHERE id = :id';

        $sql_params = [
            'id'=>$this->id
        ];

        $cache      = \xtetis\xengine\libraries\Cache::getCacheObj();
        // Генерируем ID кеша
        $cache_id = $cache->genSqlCacheId($sql, $sql_params, [$this->table_name]);

        $row = $cache->get($cache_id);
        if ($row)
        {
            // Логируем SQL
            \xtetis\xengine\models\LoggerModel::logSql(
                $sql,
                [$this->table_name],
                [
                    'use_cache'  => 1,
                    'cache_id'   => $cache_id,
                    'from_cache' => 1,
                    'time'       => (microtime(true) - $start_microtime),
                ]
            );

            return $row;
        }


        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id', $this->id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();


        // Логируем SQL
        \xtetis\xengine\models\LoggerModel::logSql(
            $sql,
            $sql_params,
            [
                'use_cache'  => 1,
                'cache_id'   => $cache_id,
                'from_cache' => 0,
                'time'       => (microtime(true) - $start_microtime),
            ]
        );

        $cache->set($cache_id, $row);

        return $row;
    }

    /**
     * Проверяет существование таблицы
     */
    public function getTableExists()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Время начала работы запроса
        $start_microtime = microtime(true);

        $this->table_name = strval($this->table_name);

        $sql = 'show tables like :table_name';
        $cache      = \xtetis\xengine\libraries\Cache::getCacheObj();
        
        // Генерируем ID кеша
        $cache_id = $cache->genSqlCacheId($sql, [$this->table_name], [$this->table_name]);

        $row = $cache->get($cache_id);
        if ($row)
        {
            // Логируем SQL
            \xtetis\xengine\models\LoggerModel::logSql(
                $sql,
                [$this->table_name],
                [
                    'use_cache'  => 1,
                    'cache_id'   => $cache_id,
                    'from_cache' => 1,
                    'time'       => (microtime(true) - $start_microtime),
                ]
            );

            return $row;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('table_name', $this->table_name, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->execute();
        $row = $stmt->fetch();


        // Логируем SQL
        \xtetis\xengine\models\LoggerModel::logSql(
            $sql,
            [$this->table_name],
            [
                'use_cache'  => 1,
                'cache_id'   => $cache_id,
                'from_cache' => 0,
                'time'       => (microtime(true) - $start_microtime),
            ]
        );

        $cache->set($cache_id, $row);


        return $row;
    }

    /**
     * Возвращает дапнные по полям таблицы
     */
    public function getTableFieldsInfo()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
        $stmt    = $connect->prepare('SHOW COLUMNS FROM  ' . $this->table_name);

        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }

    /**
     * Обновляет запись таблицы по ID
     */
    public function updateTableRecordById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->prepareFieldDataToInsertUpdate();
        if (false === $this->fields_to_update)
        {
            return false;
        }

        if (!$this->validateId())
        {
            return false;
        }

        if ($this->fields_to_update)
        {
            $flist = [];

            foreach ($this->fields_to_update as $field_name => $field_info)
            {
                $flist[] = $field_name . ' = :' . $field_name;
            }

            if (!$flist)
            {
                $this->addError('flist', 'Список полей для обновления - пустой ' . $this->table_name);

                return false;

            }

            $sql = ' UPDATE ' . $this->table_name . ' SET ' . implode(', ', $flist) .
            ' WHERE id=' . $this->id;

            $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
            $stmt    = $connect->prepare($sql);



            foreach ($this->fields_to_update as $field_name => $field_info)
            {
                $field_type = \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT;
                if ('text' == $field_info['type'])
                {
                    $field_type = \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT;
                }

                if ($field_info['limit'])
                {
                    $stmt->bindParam($field_name, $field_info['value'], $field_type, $field_info['limit']);
                }
                else
                {
                    $stmt->bindParam($field_name, $field_info['value'], $field_type);
                }

            }

            $stmt->execute();

            // Удаляем кеш согласно тегу
            $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
            $cache->deleteByTag($this->table_name);

        }

        return true;
    }

    /**
     * Подготавливает значение для вставки/обновления в таблицу
     * Получает типы полей и подготавливает значение.
     * Поддерживает
     *  - NULL
     *  - int
     *  - text
     *  - date
     *  - varchar
     * и длины полей
     */
    public function prepareFieldDataToInsertUpdate()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateTable())
        {
            return false;
        }

        $this->result_sql['getTableFieldsInfo'] = $this->getTableFieldsInfo();

        if (!$this->result_sql['getTableFieldsInfo'])
        {
            $this->addError('getTableFieldsInfo', 'Не удалось получить данные полей для таблицы ' . $this->table_name);

            return false;
        }

        $this->fields_to_update = [];

        foreach ($this->result_sql['getTableFieldsInfo'] as $row_number => $row)
        {

            if (property_exists($this, $row['Field']))
            {
                if ('id' == $row['Field'])
                {
                    continue;
                }

                if (count($this->insert_update_field_list))
                {
                    if (!in_array($row['Field'], $this->insert_update_field_list))
                    {
                        continue;
                    }
                }

                $field_name = $row['Field'];
                $value      = $this->$field_name;
                $type       = '';
                $limit      = 0;

                if ('int' == $row['Type'])
                {

                    $value = intval($value);
                    $type  = 'int';
                }

                if (strpos($row['Type'], 'int(') !== false)
                {
                    $value = intval($value);
                    $type  = 'int';
                }

                if ('text' == $row['Type'])
                {
                    $value = strval($value);
                    $type  = 'text';
                }

                if ('date' == $row['Type'])
                {
                    $value = date('Y-m-d', strtotime(strval($value)));
                    $type  = 'text';
                }

                if ('timestamp' == $row['Type'])
                {
                    $value = date('Y-m-d H:i:s', strtotime(strval($value)));
                    $type  = 'text';
                }

                if (strpos($row['Type'], 'varchar') !== false)
                {
                    $value = strval($value);
                    $type  = 'text';
                    $limit = intval(preg_replace('/[^0-9]/', '', $row['Type']));

                    if ($limit)
                    {
                        $value = mb_substr($value, 0, $limit);
                    }
                }

                if (null === $this->$field_name)
                {
                    $value = null;
                }

                if (strlen($type))
                {
                    $this->fields_to_update[$row['Field']] = [
                        'value' => $value,
                        'type'  => 'text',
                        'limit' => $limit,
                    ];
                }

            }
        }

        return true;
    }

    /**
     * Удаляет запись таблицы по ID
     */
    public function insertTableRecord()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->prepareFieldDataToInsertUpdate();
        if (false === $this->fields_to_update)
        {
            return false;
        }

        if ($this->fields_to_update)
        {
            $flist  = [];
            $fvlist = [];
            foreach ($this->fields_to_update as $field_name => $field_info)
            {
                $flist[]  = $field_name;
                $fvlist[] = ' :' . $field_name;
            }

            $sql = 'INSERT INTO ' . $this->table_name . ' (' .
            implode(', ', $flist) . ' ) VALUES ( ' .
            implode(', ', $fvlist) . ' );';

            $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
            $stmt    = $connect->prepare($sql);

            foreach ($this->fields_to_update as $field_name => $field_info)
            {
                $field_type = \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT;
                if ('text' == $field_info['type'])
                {
                    $field_type = \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT;
                }

                if ($field_info['limit'])
                {
                    $stmt->bindParam($field_name, $field_info['value'], $field_type, $field_info['limit']);
                }
                else
                {
                    $stmt->bindParam($field_name, $field_info['value'], $field_type);
                }

            }

            try
            {
                $stmt->execute();

                // Удаляем кеш согласно тегу
                $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
                $cache->deleteByTag($this->table_name);

            }
            catch (\Exception $e)
            {
                $this->addError('common', $e->getMessage());

                return false;
            }

        }

        if (!$this->setLastInsertedId())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает последний ставленный ID
     */
    public function setLastInsertedId()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateTable())
        {
            return false;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
        $stmt    = $connect->prepare('SELECT LAST_INSERT_ID() as id');

        $stmt->execute();
        $row = $stmt->fetch();

        if ($row)
        {
            $this->id = $row['id'];
        }
        else
        {
            $this->addError('id', 'Запись не найдена');

            return false;
        }

        return true;
    }

    /**
     * Удаляет запись таблицы по ID
     */
    public function deleteTableRecordById()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->validateId())
        {
            return false;
        }

        if (!$this->validateTable())
        {
            return false;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($this->connect);
        $stmt    = $connect->prepare('DELETE FROM ' . $this->table_name . ' WHERE id = :id');

        $stmt->bindParam('id', $this->id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        // Удаляем кеш согласно тегу
        $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
        $cache->deleteByTag($this->table_name);

        return $row;
    }

    /**
     * Возвращает список моделей по SQL запросу
     * Обязательным в SQL запросе является наличие поля id
     *
     * $sql = "SELECT id FROM ... WHERE param = :param_name ...."
     *
     * $params = [
     *      'param_name' => $param_value
     * ]
     */
    public static function getModelListBySql(
        $sql = '',
        $sql_params = [],
        $params = []
    )
    {
        // Замеряем время начала работы запроса
        $start_microtime = microtime(true);

        // Кешируемый ли запрос
        $use_cache  = (isset($params['cache']) && ($params['cache']));
        
        $cache      = \xtetis\xengine\libraries\Cache::getCacheObj();

        // Генерируем ID кеша
        $cache_id = $cache->genSqlCacheId($sql, $sql_params, (isset($params['cache_tags'])?$params['cache_tags']:[]));

        $called_class = get_called_class();
        $model        = new $called_class;

        // Если запрос кешируемый
        if ($use_cache)
        {
            $rows = $cache->get($cache_id);

            if ($rows)
            {

                // Логируем SQL
                \xtetis\xengine\models\LoggerModel::logSql(
                    $sql,
                    $sql_params,
                    [
                        'use_cache'  => intval($use_cache),
                        'cache_id'   => $cache_id,
                        'from_cache' => 1,
                        'time'       => (microtime(true) - $start_microtime),
                    ]
                );

                if (!$rows)
                {
                    return [];
                }
        
                $model_list = [];
                foreach ($rows as $row)
                {
                    if (!isset($row['id']))
                    {
                        throw new \Exception('Запрос ' . $sql . ' не возвращает ID');
        
                        return false;
                    }
                    $model_list[$row['id']] = $called_class::generateModelById($row['id']);
                }

                return $model_list;
            
            }
        }




        if (!$model->validateTable())
        {
            return false;
        }

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($model->connect);
        $stmt    = $connect->prepare($sql);

        foreach ($sql_params as $key => $value)
        {
            if (is_integer($value))
            {
                $stmt->bindParam($key, $value, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT);

            }
            else
            {
                $stmt->bindParam($key, $value);
            }
        }
        $stmt->execute();
        $rows = $stmt->fetchAll();


        // Логируем SQL
        \xtetis\xengine\models\LoggerModel::logSql(
            $sql,
            $sql_params,
            [
                'use_cache'  => intval($use_cache),
                'cache_id'   => $cache_id,
                'from_cache' => 0,
                'time'       => (microtime(true) - $start_microtime),
            ]
        );

        if ($use_cache)
        {
            $cache->set($cache_id, $rows);
        }

        if (!$rows)
        {
            return [];
        }

        $model_list = [];
        foreach ($rows as $row)
        {
            if (!isset($row['id']))
            {
                throw new \Exception('Запрос ' . $sql . ' не возвращает ID');

                return false;
            }
            $model_list[$row['id']] = $called_class::generateModelById($row['id']);
        }

        return $model_list;
    }

    /**
     * Возвращает количество записей в таблице по SQL запросу
     * Обязательным в SQL запросе является наличие поля count
     *
     * $sql = "SELECT COUNT(*) count FROM ... WHERE param = :param_name ...."
     *
     * $sql_params = [
     *      'param_name' => $param_value
     * ]
     *
     * $params = [
     *      'cache'=>true/false
     *      'cache_tags'=>[
     *          'tag1',
     *          ...
     *      ],
     *      'cache_expire'=>3600
     * ]
     */
    public static function getCountBySql(
        $sql = '',
        $sql_params = [],
        $params = []
    )
    {
        // Замеряем время исполнения запроса (старт)
        $start_microtime = microtime(true);

        $cache      = \xtetis\xengine\libraries\Cache::getCacheObj();

        // Кешируемый ли запров
        $use_cache  = (isset($params['cache']) && ($params['cache']));

        // Генерируем ID кеша
        $cache_id = $cache->genSqlCacheId($sql, $sql_params, (isset($params['cache_tags'])?$params['cache_tags']:[]));
        

        $called_class = get_called_class();
        $model        = new $called_class;

        // Проверяем существование таблицы
        if (!$model->validateTable())
        {
            return false;
        }

        // Если запрос кешируемый
        if ($use_cache)
        {   
            // Пытаемся получить данные из кеша по ID
            $row = $cache->get($cache_id);

            // Если данные получены
            if ($row)
            {
                // Логируем SQL (пуская и данные из кеша)
                \xtetis\xengine\models\LoggerModel::logSql(
                    $sql,
                    $sql_params,
                    [
                        'use_cache'  => intval($use_cache),
                        'cache_id'   => $cache_id,
                        'from_cache' => 1,
                        'time'       => (microtime(true) - $start_microtime),
                    ]
                );

                // Возвращаем данные
                if (isset($row['count']))
                {
                    $ret = intval($row['count']);

                    return $ret;
                }
            }
        }

        
        // Коннект к БД
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect($model->connect);
        $stmt    = $connect->prepare($sql);

        foreach ($sql_params as $key => $value)
        {
            if (is_integer($value))
            {
                $stmt->bindParam($key, $value, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT);

            }
            else
            {
                $stmt->bindParam($key, $value);
            }
        }
        $stmt->execute();
        $row = $stmt->fetch();


        // Логируем SQL
        \xtetis\xengine\models\LoggerModel::logSql(
            $sql,
            $sql_params,
            [
                'use_cache'  => intval($use_cache),
                'cache_id'   => $cache_id,
                'from_cache' => 0,
                'time'       => (microtime(true) - $start_microtime),
            ]
        );

        if (!$row)
        {
            return 0;
        }

        if (!isset($row['count']))
        {
            throw new \Exception('Запрос ' . $sql . ' не возвращает count');

            return false;
        }

        // Если запрос кешируемый
        if ($use_cache)
        {
            // Кешируем данные
            $cache->set($cache_id, $row);
        }

        $ret = intval($row['count']);



        return $ret;
    }

    /**
     * Возвращает список моделей и количество записей в таблице для пагинации
     * Используется для вывода списка записей
     */
    public static function getListModelsParams(
        $params = []
    )
    {
        $called_class = get_called_class();
        $model        = new $called_class;

        if (!$model->validateTable())
        {
            return false;
        }

        if (!isset($params['limit']))
        {
            $params['limit'] = 20;
        }
        $params['limit'] = intval($params['limit']);

        if (!isset($params['offset']))
        {
            $params['offset'] = 0;
        }
        $params['offset'] = intval($params['offset']);

        $where = ['TRUE'];

        if (isset($params['where']))
        {
            $where_array = $params['where'];
            if (is_array($where_array))
            {
                foreach ($where_array as $key => $value)
                {
                    $where[] = $value;
                }
            }

        }

        $order = '';
        if (isset($params['order']))
        {
            $order = ' ORDER BY '.strval($params['order']).' ';
        }

        $sql = '
        SELECT id 
        FROM ' . $model->table_name . ' '.
        'WHERE ' . implode(' AND ', $where) . ' '.
        $order.
        'LIMIT ' . $params['limit'] . ' '.
        'OFFSET ' . $params['offset'] . '';

        // Получаем список моделей
        $model_list = self::getModelListBySql(
            $sql, 
            [],
            [
                'cache'      => true,
                'cache_tags' => [
                    $model->table_name,
                ],
            ]
        );

        $sql = '
        SELECT COUNT(*) count 
        FROM ' . $model->table_name . ' 
        WHERE ' . implode(' AND ', $where) . '
        ';

        // Получаем количество
        $count = self::getCountBySql(
                $sql,
                [], 
                [
                    'cache'      => true,
                    'cache_tags' => [
                        $model->table_name,
                    ],
                ]
        );

        return [
            'models' => $model_list,
            'count'  => $count,
        ];
    }

}
