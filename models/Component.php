<?php

namespace xtetis\xengine\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 * Модель Component для наследования
 */
class Component extends \xtetis\xengine\models\Model
{
    /**
     * @var string
     */
    public $pre_run_callback = '';
    /**
     * Имя компонента
     */
    public $component = '';

    /**
     * @var string
     */
    public $action = '';
    /**
     * @var string
     */
    public $query = '';
    /**
     * @var string
     */
    public $request = '';

    /**
     * Директория компонента
     */
    public $component_directory = '';

    /**
     * @var mixed
     */
    public $request_filename;

    /**
     * Список констант, которые должны проверяться в контроллере/вьюхе для корректной работы
     */
    public $required_constants = [];



    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        foreach ($params as $k => $v)
        {
            if (property_exists($this, $k))
            {
                $this->$k = $v;
            }
        }
    }

    /**
     * Список роутов для компонента (не APP)
     * Для APP роуты берутся из /engine/config/params.routes.php 
     * В самих компонентах роуты можно переназначать
     */
    public static function getComponentRoutes()
    {

        $routes = [
            [
                'url'      => '/^\/(' . self::getComponentUrl(). ')(\/(\w+)?(\/(\w+)?(\/(\d+)?)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = isset($params[4]) ? $params[4] : '';

                    $id  = isset($params[6]) ? $params[6] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
        ];

        return $routes;
    }

    /**
     * Возвращает директорию компонента
     */
    public static function getComponentDirectory()
    {
        $class_name = get_called_class();
        if ('xtetis\xengine\Component' == $class_name)
        {
            $compontnt_directory = ENGINE_DIRECTORY;
        }
        else
        {
            $reflector           = new \ReflectionClass($class_name);
            $compontnt_directory = dirname($reflector->getFileName());
        }

        return $compontnt_directory;
    }

    /**
     * Запускает компонент
     */
    public function runComponent()
    {
        $this->component = strval($this->component);
        $this->action    = strval($this->action);
        $this->query     = strval($this->query);

        $this->component = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->component);
        $this->action    = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->action);
        $this->query     = preg_replace('/[^a-zA-Z0-9_]+/', '', $this->query);

        if (!strlen($this->component))
        {
            $this->addError('component', __FUNCTION__ . ': Не указан параметр component');

            return false;
        }

        if (!strlen($this->action))
        {
            $this->addError('action', __FUNCTION__ . ': Не указан параметр action');

            return false;
        }

        if (!strlen($this->query))
        {
            $this->addError('query', __FUNCTION__ . ': Не указан параметр query');

            return false;
        }

        $this->request = $this->getActionQuery();

        // Запускаем перед выполнением всего
        $this->preRun();
        


        $this->execAction();
    }





    /**
     * Выполняет функцию, указанную в PRE_RUN_METHOD (обычно это для логирования или RBAC)
     * выполняется перед выполнением action/auery или module/module_action
     */
    public function preRun()
    {
        if (defined('PRE_RUN_METHOD'))
        {
            $this->pre_run_callback = PRE_RUN_METHOD;
            if (is_string($this->pre_run_callback))
            {
                if (strlen($this->pre_run_callback))
                {
                    if (is_callable($this->pre_run_callback))
                    {
                        call_user_func($this->pre_run_callback);
                    }
                    else
                    {
                        \xtetis\xengine\helpers\LogHelper::customDie(__FUNCTION__ . ': PRE_RUN_METHOD is not callable');
                    }
                }
            }
        }

        return $this;
    }



    public function execAction()
    {

        $this->request_filename = $this::getComponentDirectory() . '/action/' . $this->request . '.php';

        if (!file_exists($this->request_filename))
        {
            throw new \Exception('Запрос не найден ' . (\xtetis\xengine\Config::isTest() ? $this->request_filename : ''), 404);

        }


        $file_content = file_get_contents($this->request_filename);

        if (strpos($file_content, "defined('SYSTEM')") === false)
        {
            throw new \Exception('В исполняемом файле не найдено определение SYSTEM ' . (\xtetis\xengine\Config::isTest() ? $this->request_filename : ''));
        }

        // Проверка наличия в коде контроллера проверки на определение константы
        // Сделано для того, чтобы некоторые контроллеры можно 
        // было запустить только определенным образом (например, админские)
        foreach ($this->required_constants as $constant_name) 
        {
            if (strpos($file_content, "defined('".$constant_name."')") === false)
            {
                throw new \Exception('В контроллере не найдена проверка на определение константы '.$constant_name .' '. (\xtetis\xengine\Config::isTest() ? $this->request_filename : ''));
            }
        }


        require_once $this->request_filename;
    }

    /**
     * ВОзвращает урл компонента. Если Это компонент APP - возвращается пустая строка
     * Если это неизвестный компонент - возвращается пустая строка
     */
    public static function getComponentUrl()
    {
        foreach (\xtetis\xengine\Config::getComponentUrlList() as $url => $class_name)
        {
            if (get_called_class() == $class_name)
            {
                return $url;
            }
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return mixed
     */
    public function getActionQuery()
    {
        return $this->getAction() . '/' . $this->getQuery();
    }

    /**
     * @param $action_query
     */
    public function isActiveActionQuery($action_query)
    {
        return ($this->getActionQuery() == $action_query);
    }

    /**
     * Проверяет является ли строка классом компонента
     */
    public static function isChildOfComponent($component_class = false)
    {

        if (!is_string($component_class))
        {
            return false;
        }

        if (!class_exists($component_class))
        {
            return false;
        }

        $component_parent_class_name = get_parent_class($component_class);

        if (!$component_parent_class_name)
        {
            return false;
        }

        if (get_class(new self()) != $component_parent_class_name)
        {
            return false;
        }

        return true;
    }

    /**
     * Генерирует урл для указанного компонента. Начало урла формируется из COMPONENT_URL_LIST
     *
     * @param array $params
     */
    public static function makeUrl($params = [
        'path'      => [],
        'query'     => [],
        'with_host' => false,
    ])
    {
        return \xtetis\xengine\helpers\UrlHelper::makeUrl($params, get_called_class());
    }

    /**
     * Рендерит блок в компоненте
     */
    public static function renderBlock(
        $block,
        $render_data = []
    )
    {
        return \xtetis\xengine\helpers\RenderHelper::renderBlock($block, $render_data, get_called_class());
    }

    /**
     * Возвращает класс текущего компонента
     */
    public static function getComponentClass()
    {
        return get_called_class();
    }

    /**
     * Возвращает список ссылок для менеджера (xcms)
     * так, например эти ссылки будут встроены в админку xcms и будут иметь вид
     * 
     * https://lsfinder.com/manager/component_admin/diary/admin/community
     * и будут требовать в контроллере проверку 
     * 
     * // Без обращения к xcms - работа невозможн
     * if (!defined('ADMIN')
     * {
     *      die('Разрешен просмотр только из XCMS');
     * }
     */
    public static function getManagerActions()
    {
        return [];
    }


    /**
     * Возвращает список урлов для построения карты кафта в формате
     * [
     *      'URL' => 'ANCHOR',
     *      ...
     * ]
     * 
     * Так, для sitemap.xml модно использовать только ключи массивов
     * А для HTML карты можно использовать как ключи, так и анкоры
     */
    public static function getSitemapUrlList():array
    {
        return [];
    }

}
