<?php

namespace xtetis\xengine\models;

/**
 * Чтобы не создавать заново модель
 */
global $logger;

/**
 * Модель LoggerModel для логирования
 */
class LoggerModel extends \xtetis\xengine\models\Model
{
    /**
     * Логи SQL
     */
    public $sql_log = [];

    /**
     * Количество SQL запросов
     */
    public $sql_request_count = 0;

    /**
     * Количество SQL запросов, где данные взяты из кеша
     */
    public $sql_from_cache_count = 0;

    /**
     * Конструктор
     */
    public function __construct($params = [])
    {
        global $logger;
        $logger = $this;
    }

    /**
     * Возвращает объект APP
     */
    public static function getLogger()
    {
        /**
         * Чтобы не создавать заново модель
         */
        global $logger;

        if (!isset($logger))
        {
            $logger = new self();
        }

        return $logger;
    }

    /**
     * Логируем SQL
     */
    public static function logSql(
        $sql = '',
        $sql_params = [],
        $params = []
    )
    {
        $model            = self::getLogger();
        $model->sql_log[] = [
            'sql'        => $sql,
            'sql_params' => $sql_params,
            'params'     => $params,
        ];

        $model->sql_request_count++;

        if ((isset($params['from_cache'])) && ($params['from_cache']))
        {
            $model->sql_from_cache_count++;
        }

    }

}
