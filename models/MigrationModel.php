<?php

namespace xtetis\xengine\models;

/**
 * Модель 
 */
class MigrationModel extends \xtetis\xengine\models\Model
{

    public function getComponentListToMigrate()
    {
        $component_url_list = \xtetis\xengine\Config::getComponentUrlList();

        return $component_url_list;
    }
}
