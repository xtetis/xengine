<?php

namespace xtetis\xengine\interfaces;

interface Module
{
    /**
     * 
     * @param $module_action
     */
    public function runModuleAction($module_action = '');

    /**
     * Возвращает урл страницы/действия модуля
     * 
     * @param string $module_action
     * @param array $query
     * @param bool $with_host
     */
    public function getModuleUrl(
        string $module_action = '',
        array  $query = [],
        bool   $with_host = false
    );
}
