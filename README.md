# xEngine
## Фреймворк для разных задач на PHP

Powered by xTetis

Легковесный фреймворк для создания PHP приложения

## Функции

- Работа с SEF (роуты настраиваются с помощью регулярок)
- Рендеринг шаблонов/блоков/страниц
- Поддержка модулей (смотрите модули для xEngine на https://bitbucket.org/xtetis/workspace/projects/XEN)
---
## Генерация/обновление документации с помощью phpDocumentor

Если еще не установлен phpDocumentor - скачиваем https://phpdoc.org/phpDocumentor.phar 
В директории со скачанным phpDocumentor.phar  выполняем команды
```sh
chmod +x phpDocumentor.phar
mv phpDocumentor.phar /usr/local/bin/phpDocumentor
```

Для генерации/обновления в директории vendor/xtetis/xengine запускаем команду 
```sh
phpDocumentor -d . -t docs
```
---
Править далее документацию можно на https://dillinger.io/
